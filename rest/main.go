package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"rest/scamdbpb"
	"rest/scammodelpb"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ScoreRequest struct {
	EventID   uuid.UUID `json:"eventId"`
	ANumber   string    `json:"aNumber"`
	BNumber   string    `json:"bNumber"`
	SipInvite string    `json:"sipInvite"`
}

type Score struct {
	EventID   uuid.UUID              `json:"eventId"`
	IsScam    bool                   `json:"isScam"`
	BlockCall bool                   `json:"blockCall"`
	Metadata  map[string]interface{} `json:"metadata"`
	Score     int64                  `json:"-"` // never return this
}

type ResultEvent struct {
	ANumber  string `json:"aNumber"`
	BNumber  string `json:"bNumber"`
	CallTime int64  `json:"callTime"`
}

type Result struct {
	EventID   string      `json:"eventId"`
	Event     ResultEvent `json:"event"`
	BlockCall bool        `json:"blockCall"`
	IsScam    bool        `json:"isScam"`
	Score     int64       `json:"score"`
}

type ResultResponse struct {
	Body []Result `json:"body"`
}

type Lookup struct {
	CategoryCode      string   `json:"categoryCode"`
	CategoryName      string   `json:"categoryName"`
	BlockedANumbers   []string `json:"blockedAnumbers"`
	BlockedCategories []string `json:"blockedCategories"`
}

type LookupResponse struct {
	Body Lookup `json:"body"`
}

type ScoreBatchResponse struct {
	Body []Score `json:"body"`
}

type ScoreResponse struct {
	Body Score `json:"body"`
}

type Aggregate struct {
	PercentBlocked      int64  `json:"percentBlocked"`
	AverageScore        int64  `json:"averageScore"`
	MostBlockedCategory string `json:"mostBlockedCategory"`
}

type AggregateResponse struct {
	Body Aggregate `json:"body"`
}

type ClientController struct {
	vn *grpc.ClientConn
	bl *grpc.ClientConn
	td *grpc.ClientConn
	sd *grpc.ClientConn

	vnc scammodelpb.ScamServiceClient
	blc scammodelpb.ScamServiceClient
	tdc scammodelpb.ScamServiceClient
	sdc scamdbpb.ScamDBClient
}

func main() {
	ascii()
	cc := newController()
	httpStart(cc)
}

func httpStart(cc *ClientController) {
	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(":7000", nil)

	router := gin.Default()
	router.Use(cors.Default())

	router.POST("/api/call-protection/calls", cc.handleCalls)
	router.POST("/api/call-protection/batch", cc.handleBatch)
	router.GET("/api/call-protection/results", cc.handleResults)
	router.GET("/api/call-protection/results/:phoneNumber", cc.handleResults)
	router.GET("/api/call-protection/phone/:phoneNumber", cc.handleLookup)
	router.GET("/api/call-protection/aggregates", cc.handleAggregates)
	router.PUT("/api/call-protection/phone/:phoneNumber", cc.handleUpdate)

	router.GET("/health", func(c *gin.Context) {
		c.String(http.StatusOK, "OK")
	})
	router.Run(":8080")
}

/**** ClientController initialization functions ****/

func newController() *ClientController {
	cc := &ClientController{}
	cc.grpcStart()
	return cc
}

func (cc *ClientController) grpcStart() {
	var (
		VALNUM_HOST    = getEnvOrElse("VALNUM_HOST", "localhost")
		VALNUM_PORT    = getEnvOrElse("VALNUM_PORT", "50061")
		BLOCKLIST_HOST = getEnvOrElse("BLOCKLIST_HOST", "localhost")
		BLOCKLIST_PORT = getEnvOrElse("BLOCKLIST_PORT", "50071")
		TOD_HOST       = getEnvOrElse("TOD_HOST", "localhost")
		TOD_PORT       = getEnvOrElse("TOD_PORT", "50081")
		SCAMDB_HOST    = getEnvOrElse("SCAMDB_HOST", "localhost")
		SCAMDB_PORT    = getEnvOrElse("SCAMDB_PORT", "50051")
	)

	cc.vn = dial("valnum", VALNUM_HOST, VALNUM_PORT)
	cc.vnc = scammodelpb.NewScamServiceClient(cc.vn)

	cc.bl = dial("blocklist", BLOCKLIST_HOST, BLOCKLIST_PORT)
	cc.blc = scammodelpb.NewScamServiceClient(cc.bl)

	cc.td = dial("timeofday", TOD_HOST, TOD_PORT)
	cc.tdc = scammodelpb.NewScamServiceClient(cc.td)

	cc.sd = dial("scamdb", SCAMDB_HOST, SCAMDB_PORT)
	cc.sdc = scamdbpb.NewScamDBClient(cc.sd)
}

func dial(svcName string, host string, port string) *grpc.ClientConn {
	creds := grpc.WithTransportCredentials(insecure.NewCredentials())
	conn, err := grpc.Dial(fmt.Sprintf("%s:%s", host, port), creds)
	if err != nil {
		log.Fatalf("Error initializing %v client: %v", svcName, err)
	}
	return conn
}

func getEnvOrElse(key string, orElse string) string {
	val, exists := os.LookupEnv(key)
	if exists {
		return val
	}
	return orElse
}

/**** HTTP handler and helper functions ****/

func badRequest(c *gin.Context, err error) {
	c.JSON(http.StatusBadRequest, gin.H{
		"Bad Request": err.Error(),
	})
}

func (cc *ClientController) handleCalls(c *gin.Context) {
	req := ScoreRequest{}
	if err := c.ShouldBindJSON(&req); err != nil {
		badRequest(c, err)
	} else {
		var (
			valnumCh = make(chan Score, 1)
			blockCh  = make(chan Score, 1)
			todCh    = make(chan Score, 1)
		)

		go scamModelUnaryCall(c, cc.blc, &req, blockCh)
		go scamModelUnaryCall(c, cc.vnc, &req, valnumCh)
		go scamModelUnaryCall(c, cc.tdc, &req, todCh)

		var res ScoreResponse
		res.Body = mergeResponses(<-valnumCh, <-blockCh, <-todCh)

		_, err = cc.sdc.PutScoringResult(c, buildRequest(res.Body, req))
		if err != nil {
			log.Printf("Error persisting results: %v", err)
		}
		c.JSON(http.StatusOK, res)
	}
}

func (cc *ClientController) handleBatch(c *gin.Context) {
	var req []ScoreRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		badRequest(c, err)
	} else {
		var (
			valnumCh = make(chan Score, 1)
			blockCh  = make(chan Score, 1)
			todCh    = make(chan Score, 1)
		)

		stream, err := cc.sdc.PutScoringResults(c)
		if err != nil {
			log.Printf("Could not call PutScoringResults RPC: %v", err)
		}

		go scamModelStreamCall(c, cc.vnc, req, valnumCh)
		go scamModelStreamCall(c, cc.blc, req, blockCh)
		go scamModelStreamCall(c, cc.tdc, req, todCh)

		var res ScoreBatchResponse
		res.Body = make([]Score, len(req))
		for i := range req {
			res.Body[i] = mergeResponses(<-valnumCh, <-blockCh, <-todCh)
			err = stream.Send(buildRequest(res.Body[i], req[i]))
			if err != nil {
				log.Printf("Error persisting result: %v", err)
			}
		}
		c.JSON(http.StatusOK, res)
	}
}

func defaultInt64Query(c *gin.Context, query string, def string) int64 {
	ret, err := strconv.Atoi(c.DefaultQuery(query, def))
	if err != nil {
		badRequest(c, err)
	}
	return int64(ret)
}

func (cc *ClientController) handleResults(c *gin.Context) {
	limit := defaultInt64Query(c, "limit", "10")
	offset := defaultInt64Query(c, "offset", "0")

	pNumber := c.Param("phoneNumber")
	if pNumber == "" {
		pNumber = ".*"
	}

	time := time.Now().Unix()
	results, err := cc.sdc.ListScoringResults(c, &scamdbpb.ListScoringResultsRequest{
		Limit:             limit,
		Offset:            offset,
		TimeFrom:          time,
		TimeOffset:        time,
		SearchPhoneNumber: pNumber,
	})
	if err != nil {
		log.Printf("Could not call ListScoringResults gRPC service: %v", err)
	}

	resp := ResultResponse{Body: []Result{}}
	for _, result := range results.ScoringResults {
		resp.Body = append(resp.Body, Result{
			EventID: result.GetEventId(),
			Event: ResultEvent{
				ANumber:  result.GetEvent().GetANumber(),
				BNumber:  result.GetEvent().GetBNumber(),
				CallTime: result.GetEvent().GetCallTime(),
			},
			BlockCall: result.GetBlockCall(),
			IsScam:    result.GetIsScam(),
			Score:     result.GetScore(),
		})
	}

	c.JSON(http.StatusOK, resp)
}

func (cc *ClientController) handleLookup(c *gin.Context) {
	pNumber := c.Param("phoneNumber")

	blockRes, blockErr := cc.sdc.GetPersonalBlocklist(context.Background(), &scamdbpb.GetPersonalBlocklistRequest{
		PhoneNumber: pNumber,
	})
	if blockErr != nil {
		log.Printf("Error while calling ScamDatabase: %v", blockErr)
	}
	catRes, catErr := cc.sdc.GetPhoneCategory(context.Background(), &scamdbpb.GetPhoneCategoryRequest{
		PhoneNumber: pNumber,
	})
	if catErr != nil {
		log.Printf("Error while calling ScamDatabase: %v", blockErr)
	}

	c.JSON(http.StatusOK, LookupResponse{
		Body: Lookup{
			CategoryCode:      catRes.GetCategoryCode(),
			CategoryName:      catRes.GetCategoryName(),
			BlockedANumbers:   blockRes.GetBlockedANumbers(),
			BlockedCategories: blockRes.GetBlockedCategories(),
		},
	})
}

func (cc *ClientController) handleAggregates(c *gin.Context) {
	curTime := time.Now().Unix()
	results, err := cc.sdc.ListScoringResults(c, &scamdbpb.ListScoringResultsRequest{
		Limit:      0,
		Offset:     0,
		TimeFrom:   curTime,
		TimeOffset: int64(24 * time.Hour.Seconds()),
	})
	log.Printf("Pulling results with timestamp starting at %v", curTime-60*60)
	if err != nil {
		log.Printf("Could not call ListScoringResults gRPC service: %v", err)
	}
	aggregate := cc.calcAggregates(results.ScoringResults)

	c.JSON(http.StatusOK, AggregateResponse{aggregate})
}

func (cc *ClientController) handleUpdate(c *gin.Context) {
	req := Lookup{}
	if err := c.ShouldBindJSON(&req); err != nil {
		badRequest(c, err)
	} else {
		pNumber := c.Param("phoneNumber")
		_, err := cc.sdc.UpdatePhoneNumber(c, &scamdbpb.UpdatePhoneNumberRequest{
			PhoneNumber:       pNumber,
			CategoryCode:      req.CategoryCode,
			CategoryName:      req.CategoryName,
			BlockedANumbers:   req.BlockedANumbers,
			BlockedCategories: req.BlockedCategories,
		})
		if err != nil {
			c.Status(http.StatusServiceUnavailable)
			log.Printf("Received error from scamdb server: %v", err)
		} else {
			c.Status(http.StatusNoContent)
		}
	}
}

/**** Data manipulation helper functions ****/

func (cc *ClientController) calcAggregates(results []*scamdbpb.ScoringResult) Aggregate {
	apiResp := Aggregate{
		PercentBlocked:      0,
		AverageScore:        0,
		MostBlockedCategory: "N/A",
	}

	respLen := len(results)
	if respLen < 1 {
		return apiResp
	} else {
		aggregates := make(map[string]int)
		categories := make(map[string]int)

		aggregates["totalBlocked"] = 0
		aggregates["totalScore"] = 0

		for _, event := range results {
			if event.GetBlockCall() {
				aggregates["totalBlocked"] += 1
				catRes, _ := cc.sdc.GetPhoneCategory(context.Background(), &scamdbpb.GetPhoneCategoryRequest{
					PhoneNumber: event.Event.GetANumber(),
				})
				if catCount, ok := categories[catRes.GetCategoryCode()]; ok {
					categories[catRes.GetCategoryCode()] = catCount + 1
				} else {
					categories[catRes.GetCategoryCode()] = 1
				}
			}
			aggregates["totalScore"] += int(event.GetScore())
		}

		highestQty := 0
		mostBlockedCategory := "UN"
		for k, v := range categories {
			if v > highestQty {
				mostBlockedCategory = k
				highestQty = v
			}
		}

		apiResp.PercentBlocked = int64(aggregates["totalBlocked"] * 100 / respLen)
		apiResp.AverageScore = int64(aggregates["totalScore"] / respLen)
		apiResp.MostBlockedCategory = mostBlockedCategory
	}

	return apiResp
}

func buildRequest(res Score, req ScoreRequest) *scamdbpb.PutScoringResultRequest {
	request := &scamdbpb.PutScoringResultRequest{
		ScoringResult: &scamdbpb.ScoringResult{
			EventId: res.EventID.String(),
			Event: &scamdbpb.Event{
				ANumber:  req.ANumber,
				BNumber:  req.BNumber,
				CallTime: time.Now().Unix(),
			},
			BlockCall: res.BlockCall,
			IsScam:    res.IsScam,
			Score:     res.Score,
		},
	}
	return request
}

func mergeResponses(resp ...Score) Score {
	if len(resp) < 2 {
		return resp[0]
	}
	out := resp[0]
	for _, elem := range resp[1:] {
		out.EventID = elem.EventID
		out.IsScam = out.IsScam || elem.IsScam
		out.BlockCall = out.BlockCall || elem.BlockCall
		out.Metadata = metaMerge(out.Metadata, elem.Metadata)
		out.Score = out.Score + elem.Score
	}
	out.IsScam = out.IsScam || out.Score >= 80
	out.BlockCall = out.BlockCall || out.Score >= 90
	out.Metadata = metaMerge(out.Metadata, map[string]interface{}{
		"score": out.Score,
	})
	return out
}

func metaMerge(maps ...map[string]interface{}) map[string]interface{} {
	if len(maps) < 2 {
		return maps[0]
	}
	out := maps[0]
	for _, elem := range maps[1:] {
		for k, v := range elem {
			if _, ok := elem[k]; ok {
				out[k] = v
			}
		}
	}
	return out
}

/**** gRPC service calling functions ****/

func scamModelUnaryCall(c context.Context, sc scammodelpb.ScamServiceClient, req *ScoreRequest, ch chan Score) {
	event := &scammodelpb.ScoreEventRequest{
		EventId:   req.EventID.String(),
		ANumber:   req.ANumber,
		BNumber:   req.BNumber,
		CallTime:  time.Now().Unix(),
		SipInvite: req.SipInvite,
	}
	verdict, err := sc.ScoreEvent(c, event)
	if err != nil {
		log.Printf("There was an error calling gRPC service: %v", err)
	}
	ch <- Score{
		EventID:   req.EventID,
		BlockCall: verdict.BlockCall,
		IsScam:    verdict.IsScam,
		Score:     verdict.Score,
		Metadata:  verdict.GetMetadata().AsMap(),
	}
	log.Printf("Unary call to: %v done.", sc)
}

func scamModelStreamCall(c context.Context, sc scammodelpb.ScamServiceClient, batch []ScoreRequest, ch chan Score) {
	scoreStream, err := sc.ScoreEvents(c)
	if err != nil {
		log.Printf("There was an error calling gRPC service: %v", err)
	}
	waitc := make(chan struct{})
	go func() {
		for _, event := range batch {
			scoreStream.Send(&scammodelpb.ScoreEventRequest{
				EventId:   event.EventID.String(),
				ANumber:   event.ANumber,
				BNumber:   event.BNumber,
				CallTime:  time.Now().Unix(),
				SipInvite: event.SipInvite,
			})
		}
		scoreStream.CloseSend()
	}()
	go func() {
		for {
			res, err := scoreStream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Printf("Problem while reading server stream: %v", err)
				break
			}
			uuidStr := res.GetEventId()
			uuid, _ := uuid.Parse(uuidStr)
			ch <- Score{
				EventID:   uuid,
				IsScam:    res.GetIsScam(),
				BlockCall: res.GetBlockCall(),
				Score:     res.Score,
				Metadata:  res.GetMetadata().AsMap(),
			}
		}
		close(waitc)
	}()
	<-waitc
}

func ascii() {
	fmt.Printf(" ▄ .▄ ▄▄▄· ·▄▄▄▄  ▄▄▄ ..▄▄ · \n██▪▐█▐█ ▀█ ██▪ ██ ▀▄.▀·▐█ ▀. \n██▀▐█▄█▀▀█ ▐█· ▐█▌▐▀▀▪▄▄▀▀▀█▄\n██▌▐▀▐█ ▪▐▌██. ██ ▐█▄▄▌▐█▄▪▐█\n▀▀▀ · ▀  ▀ ▀▀▀▀▀•  ▀▀▀  ▀▀▀▀\n")
}
