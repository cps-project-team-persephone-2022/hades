package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"time"
	"timeofday/scammodelpb"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"google.golang.org/protobuf/types/known/structpb"
)

type server struct {
	scammodelpb.ScamServiceServer
}

func main() {
	ascii()
	log.Println("Starting TimeOfDay ScamModel Server...")
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	s := grpc.NewServer()
	scammodelpb.RegisterScamServiceServer(s, &server{})
	reflection.Register(s)

	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(":7000", nil)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}

func buildResponse(req *scammodelpb.ScoreEventRequest) *scammodelpb.ScoreEventResponse {
	callHour := time.Unix(req.GetCallTime(), 0).Hour()

	score := 0
	if callHour >= 17 && callHour <= 20 {
		score = 20
	} else if callHour > 20 || callHour <= 6 {
		score = 40
	}

	meta, _ := structpb.NewStruct(map[string]interface{}{
		"hourOfDay": callHour,
	})

	res := &scammodelpb.ScoreEventResponse{
		EventId:   req.GetEventId(),
		Score:     int64(score),
		BlockCall: false,
		IsScam:    false,
		Metadata:  meta,
	}

	return res
}

func (*server) ScoreEvent(ctx context.Context, req *scammodelpb.ScoreEventRequest) (*scammodelpb.ScoreEventResponse, error) {
	log.Printf("Received Call Time RPC %v", req)

	res := buildResponse(req)

	return res, nil
}

func (*server) ScoreEvents(stream scammodelpb.ScamService_ScoreEventsServer) error {
	log.Printf("ScoreEvents function was invoked with a streaming request\n")

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			log.Fatalf("Error while reading client stream: %v", err)
			return err
		}
		res := buildResponse(req)

		sendErr := stream.Send(res)

		if sendErr != nil {
			log.Fatalf("Error while sending data to client: %v", sendErr)
			return sendErr
		}
	}
}

func ascii() {
	fmt.Printf(" ▄ .▄ ▄▄▄· ·▄▄▄▄  ▄▄▄ ..▄▄ · \n██▪▐█▐█ ▀█ ██▪ ██ ▀▄.▀·▐█ ▀. \n██▀▐█▄█▀▀█ ▐█· ▐█▌▐▀▀▪▄▄▀▀▀█▄\n██▌▐▀▐█ ▪▐▌██. ██ ▐█▄▄▌▐█▄▪▐█\n▀▀▀ · ▀  ▀ ▀▀▀▀▀•  ▀▀▀  ▀▀▀▀\n")
}
