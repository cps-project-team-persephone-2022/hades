package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"

	"blocklist/scamdbpb"
	"blocklist/scammodelpb"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"google.golang.org/protobuf/types/known/structpb"
)

type server struct {
	scammodelpb.ScamServiceServer
}

type BlockList struct {
	BlockedANumbers   []string //maybe use a list
	BlockedCategories []string
}
type Category struct {
	CategoryCode string
	CategoryName string
}

var (
	SCAMDB_HOST = GetenvOrElse("SCAMDB_HOST", "localhost")
	SCAMDB_PORT = GetenvOrElse("SCAMDB_PORT", "50051")
)

var sdb scamdbpb.ScamDBClient

func GetenvOrElse(key string, orElse string) string {
	val, exists := os.LookupEnv(key)
	if exists {
		return val
	} else {
		return orElse
	}
}

func main() {
	ascii()
	fmt.Println("Starting BlockList ScamModel server...")
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}
	scamdbURI := fmt.Sprintf("%s:%s", SCAMDB_HOST, SCAMDB_PORT)
	cc, err := grpc.Dial(scamdbURI, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect: %v", err)
	}
	defer cc.Close()
	s := grpc.NewServer()
	scammodelpb.RegisterScamServiceServer(s, &server{})
	reflection.Register(s)

	sdb = scamdbpb.NewScamDBClient(cc)

	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(":7000", nil)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}

func isBlocked(aNumber string, blocklist BlockList, category Category) (bool, string) {
	if category.CategoryCode == "SC" {
		return true, "Blocked Category"
	}
	for _, blockedNum := range blocklist.BlockedANumbers {
		if aNumber == blockedNum {
			return true, "Blocked Number"
		}
	}
	for _, blockedCat := range blocklist.BlockedCategories {
		if category.CategoryCode == blockedCat {
			return true, "Blocked Category"
		}
	}
	return false, "Not on blocklist"
}

func buildResponse(req *scammodelpb.ScoreEventRequest) *scammodelpb.ScoreEventResponse {
	aNumber := req.GetANumber()
	bNumber := req.GetBNumber()
	blocklist, category := callDB(aNumber, bNumber)
	score := 0
	blockCall, result := isBlocked(aNumber, blocklist, category)
	if category.CategoryCode == "SC" {
		score = 100 // known scam caller
	} else if category.CategoryCode == "UN" {
		score = 40 // unknown caller
	} else {
		score = 0
	}
	meta, _ := structpb.NewStruct(map[string]interface{}{
		"blocklistDecision": result,
		"category":          category.CategoryName,
	})
	resp := &scammodelpb.ScoreEventResponse{
		EventId:   req.GetEventId(),
		BlockCall: blockCall,
		IsScam:    false,
		Score:     int64(score),
		Metadata:  meta,
	}
	return resp
}

func (*server) ScoreEvent(ctx context.Context, req *scammodelpb.ScoreEventRequest) (*scammodelpb.ScoreEventResponse, error) {
	resp := buildResponse(req)
	return resp, nil
}

func (*server) ScoreEvents(stream scammodelpb.ScamService_ScoreEventsServer) error {
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			log.Printf("Error while reading client stream: %v\n", err)
			return err
		}

		resp := buildResponse(req)

		sendErr := stream.Send(resp)
		if sendErr != nil {
			log.Printf("Error while reading client: %v\n", err)
			return err
		}
	}
}

func callDB(aNumber string, bNumber string) (BlockList, Category) {
	blockReq := &scamdbpb.GetPersonalBlocklistRequest{
		PhoneNumber: bNumber,
	}
	catReq := &scamdbpb.GetPhoneCategoryRequest{
		PhoneNumber: aNumber,
	}

	blockRes, blockErr := sdb.GetPersonalBlocklist(context.Background(), blockReq)
	if blockErr != nil {
		log.Printf("Error while calling ScamDatabase: %v", blockErr)
	}

	catRes, catErr := sdb.GetPhoneCategory(context.Background(), catReq)
	if catErr != nil {
		log.Printf("Error while calling ScamDatabase: %v", blockErr)
	}

	blocklist := BlockList{
		BlockedANumbers:   blockRes.GetBlockedANumbers(),
		BlockedCategories: blockRes.GetBlockedCategories(),
	}
	category := Category{
		CategoryCode: catRes.GetCategoryCode(),
		CategoryName: catRes.GetCategoryName(),
	}

	return blocklist, category
}

func ascii() {
	fmt.Printf(" ▄ .▄ ▄▄▄· ·▄▄▄▄  ▄▄▄ ..▄▄ · \n██▪▐█▐█ ▀█ ██▪ ██ ▀▄.▀·▐█ ▀. \n██▀▐█▄█▀▀█ ▐█· ▐█▌▐▀▀▪▄▄▀▀▀█▄\n██▌▐▀▐█ ▪▐▌██. ██ ▐█▄▄▌▐█▄▪▐█\n▀▀▀ · ▀  ▀ ▀▀▀▀▀•  ▀▀▀  ▀▀▀▀\n")
}
