db = new Mongo().getDB('categories');
db.createCollection('categories', { capped: false });
db.categories.insert([
  {
    phone_number: '5944153220',
    category_code: 'MD',
    category_name: 'Medical',
  },
  {
    phone_number: '6009254451',
    category_code: 'PL',
    category_name: 'Political',
  },
  {
    phone_number: '7536497082',
    category_code: 'CO',
    category_name: 'Commercial',
  },
  {
    phone_number: '3982541874',
    category_code: 'GV',
    category_name: 'Government',
  },
  {
    phone_number: '7885411765',
    category_code: 'SC',
    category_name: 'Known Scam',
  },
]);

db = new Mongo().getDB('blocklist');
db.createCollection('blocklist', { capped: false });
db.blocklist.insert([
  {
    phone_number: '6009254451',
    blocked_a_numbers: ['7885411765', '4597457391'],
    blocked_categories: ['SC', 'PL'],
  },
  {
    phone_number: '3722486816',
    blocked_a_numbers: ['7885411765'],
    blocked_categories: ['SC', 'GV'],
  },
  {
    phone_number: '2343089946',
    blocked_a_numbers: ['8605569283', '2412232583'],
    blocked_categories: ['TM', 'SC'],
  },
  {
    phone_number: '9366243190',
    blocked_a_numbers: [],
    blocked_categories: [],
  },
  {
    phone_number: '2416387831',
    blocked_a_numbers: ['2422175297'],
    blocked_categories: ['TM'],
  },
  {
    phone_number: '5023288920',
    blocked_a_numbers: ['9669473875'],
    blocked_categories: ['TM'],
  },
  {
    phone_number: '3414106398',
    blocked_a_numbers: ['4793140658'],
    blocked_categories: ['SC'],
  },
  {
    phone_number: '3392055365',
    blocked_a_numbers: ['7236462908', '6962098047', '4782511198', '9755712315'],
    blocked_categories: ['SC'],
  },
  {
    phone_number: '5745917529',
    blocked_a_numbers: ['4239905324', '3597728005', '7059441153', '8878770333', '9002943003'],
    blocked_categories: ['SC'],
  },
  {
    phone_number: '3568429479',
    blocked_a_numbers: ['8617378279'],
    blocked_categories: ['DC'],
  },
  {
    phone_number: '3077045868',
    blocked_a_numbers: [
      '6863401612',
      '7728479554',
      '5719116905',
      '4883195555',
      '9904791815',
      '9953657784',
      '4065051477',
      '7855395328',
      '6403958966',
      '9472971294',
    ],
    blocked_categories: ['GV'],
  },
  {
    phone_number: '8416801625',
    blocked_a_numbers: [],
    blocked_categories: [],
  },
  {
    phone_number: '2062046458',
    blocked_a_numbers: [],
    blocked_categories: [],
  },
]);

db = new Mongo().getDB('scoringresults');
db.createCollection('scoringresults', { capped: false });
db.scoringresults.insert([
  {
    event_id: '1ec6b0e9-e1ea-4e4d-b0bf-289817328f5f',
    event: {
      a_number: '3723305049',
      b_number: '9755535307',
      call_time: 100000,
    },
    block_call: true,
    is_scam: true,
    score: 40,
  },
]);
