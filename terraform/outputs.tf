output "s3_bucket_url" {
  value = aws_s3_bucket_website_configuration.static.website_endpoint
}

output "s3_bucket" {
  value = aws_s3_bucket.static.bucket
}