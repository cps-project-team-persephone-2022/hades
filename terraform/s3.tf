resource "aws_s3_bucket" "static" {
  bucket = "persephone-web"
}

resource "aws_s3_bucket_website_configuration" "static" {
  bucket = aws_s3_bucket.static.bucket
  index_document {
    suffix = "index.html"
  }
}
