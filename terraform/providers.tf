provider "aws" {
  region  = "us-east-2"
  profile = "618394590705"
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.grpc-eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.grpc-eks.certificate_authority[0].data)
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["eks", "get-token", "--cluster-name", data.aws_eks_cluster.grpc-eks.name]
    command     = "aws"
  }
}