resource "kubernetes_service" "mongodb" {
  metadata {
    name      = "mongodb"
    namespace = "persephone"

    labels = {
      app = "mongodb"

      team = "persephone"
    }
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 27017
      target_port = "27017"
    }

    selector = {
      app = "mongodb"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "rest" {
  metadata {
    name      = "rest"
    namespace = "persephone"

    labels = {
      app = "rest"

      team = "persephone"
    }
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 8080
      target_port = "8080"
    }

    selector = {
      app = "rest"
    }

    type = "NodePort"
  }
}

resource "kubernetes_service" "valnum" {
  metadata {
    name      = "valnum"
    namespace = "persephone"

    labels = {
      app = "valnum"

      team = "persephone"
    }
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 50051
      target_port = "50051"
    }

    selector = {
      app = "valnum"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "blocklist" {
  metadata {
    name      = "blocklist"
    namespace = "persephone"

    labels = {
      app = "blocklist"

      team = "persephone"
    }
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 50051
      target_port = "50051"
    }

    selector = {
      app = "blocklist"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "scamdb" {
  metadata {
    name      = "scamdb"
    namespace = "persephone"

    labels = {
      app = "scamdb"

      team = "persephone"
    }
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 50051
      target_port = "50051"
    }

    selector = {
      app = "scamdb"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "timeofday" {
  metadata {
    name      = "timeofday"
    namespace = "persephone"

    labels = {
      app = "timeofday"

      team = "persephone"
    }
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 50051
      target_port = "50051"
    }

    selector = {
      app = "timeofday"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "timeofday_metrics" {
  metadata {
    name      = "timeofday-metrics"
    namespace = "persephone"

    labels = {
      app = "timeofday-metrics"

      team = "persephone"
    }
  }

  spec {
    port {
      name        = "metrics"
      protocol    = "TCP"
      port        = 7000
      target_port = "7000"
    }

    selector = {
      app = "timeofday"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "rest_metrics" {
  metadata {
    name      = "rest-metrics"
    namespace = "persephone"

    labels = {
      app = "rest-metrics"

      team = "persephone"
    }
  }

  spec {
    port {
      name        = "metrics"
      protocol    = "TCP"
      port        = 7000
      target_port = "7000"
    }

    selector = {
      app = "rest"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "blocklist_metrics" {
  metadata {
    name      = "blocklist-metrics"
    namespace = "persephone"

    labels = {
      app = "blocklist-metrics"

      team = "persephone"
    }
  }

  spec {
    port {
      name        = "metrics"
      protocol    = "TCP"
      port        = 7000
      target_port = "7000"
    }

    selector = {
      app = "blocklist"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "valnum_metrics" {
  metadata {
    name      = "valnum-metrics"
    namespace = "persephone"

    labels = {
      app = "valnum-metrics"

      team = "persephone"
    }
  }

  spec {
    port {
      name        = "metrics"
      protocol    = "TCP"
      port        = 7000
      target_port = "7000"
    }

    selector = {
      app = "valnum"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "scamdb_metrics" {
  metadata {
    name      = "scamdb-metrics"
    namespace = "persephone"

    labels = {
      app = "scamdb-metrics"

      team = "persephone"
    }
  }

  spec {
    port {
      name        = "metrics"
      protocol    = "TCP"
      port        = 7000
      target_port = "7000"
    }

    selector = {
      app = "scamdb"
    }

    type = "ClusterIP"
  }
}

