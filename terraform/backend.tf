terraform {
  backend "s3" {
    bucket = "persephone-tf-state"
    key    = "tf-state"
    region = "us-east-2"
  }
}