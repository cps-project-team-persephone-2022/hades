variable "namespace" {
  type = string
  default = "persephone"
}

variable "team" {
  type = string
  default = "persephone"
}