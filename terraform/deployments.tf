resource "kubernetes_deployment" "mongodb" {
  metadata {
    name      = "mongodb"
    namespace = "persephone"

    labels = {
      app = "mongodb"

      team = "persephone"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "mongodb"
      }
    }

    template {
      metadata {
        labels = {
          app = "mongodb"

          team = "persephone"
        }
      }

      spec {
        volume {
          name = "mongoinit"

          config_map {
            name = "mongoinit"
          }
        }

        container {
          name  = "mongodb"
          image = "mongo"

          port {
            container_port = 27017
            protocol       = "TCP"
          }

          resources {
            limits = {
              cpu = "500m"

              memory = "512Mi"
            }

            requests = {
              cpu = "125m"

              memory = "128Mi"
            }
          }

          volume_mount {
            name       = "mongoinit"
            read_only  = true
            mount_path = "/docker-entrypoint-initdb.d/mongoinit.js"
          }

          image_pull_policy = "Always"
        }

        termination_grace_period_seconds = 30
      }
    }
  }
}

resource "kubernetes_deployment" "rest" {
  metadata {
    name      = "rest"
    namespace = "persephone"

    labels = {
      app = "rest"

      team = "persephone"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "rest"
      }
    }

    template {
      metadata {
        labels = {
          app = "rest"

          team = "persephone"
        }
      }

      spec {
        container {
          name  = "rest"
          image = "618394590705.dkr.ecr.us-east-2.amazonaws.com/persephone-rest"

          port {
            name           = "http"
            container_port = 8080
          }

          port {
            name           = "metrics"
            container_port = 7000
          }

          env {
            name  = "VALNUM_HOST"
            value = "valnum"
          }

          env {
            name  = "VALNUM_PORT"
            value = "50051"
          }

          env {
            name  = "BLOCKLIST_HOST"
            value = "blocklist"
          }

          env {
            name  = "BLOCKLIST_PORT"
            value = "50051"
          }

          env {
            name  = "TOD_HOST"
            value = "timeofday"
          }

          env {
            name  = "TOD_PORT"
            value = "50051"
          }

          env {
            name  = "SCAMDB_HOST"
            value = "scamdb"
          }

          env {
            name  = "SCAMDB_PORT"
            value = "50051"
          }

          resources {
            limits = {
              cpu = "1"

              memory = "1Gi"
            }

            requests = {
              cpu = "250m"

              memory = "256Mi"
            }
          }

          liveness_probe {
            http_get {
              path = "/health"
              port = "8080"
            }

            initial_delay_seconds = 5
            period_seconds        = 10
          }

          image_pull_policy = "Always"
        }

        termination_grace_period_seconds = 30
      }
    }
  }
}

resource "kubernetes_deployment" "valnum" {
  metadata {
    name      = "valnum"
    namespace = "persephone"

    labels = {
      app = "valnum"

      team = "persephone"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "valnum"
      }
    }

    template {
      metadata {
        labels = {
          app = "valnum"

          team = "persephone"
        }
      }

      spec {
        container {
          name  = "valnum"
          image = "618394590705.dkr.ecr.us-east-2.amazonaws.com/persephone-valnum"

          port {
            name           = "grpc"
            container_port = 50051
          }

          port {
            name           = "metrics"
            container_port = 7000
          }

          resources {
            limits = {
              cpu = "500m"

              memory = "512Mi"
            }

            requests = {
              cpu = "125m"

              memory = "128Mi"
            }
          }

          image_pull_policy = "Always"
        }

        termination_grace_period_seconds = 30
      }
    }
  }
}

resource "kubernetes_deployment" "blocklist" {
  metadata {
    name      = "blocklist"
    namespace = "persephone"

    labels = {
      app = "blocklist"

      team = "persephone"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "blocklist"
      }
    }

    template {
      metadata {
        labels = {
          app = "blocklist"

          team = "persephone"
        }
      }

      spec {
        container {
          name  = "blocklist"
          image = "618394590705.dkr.ecr.us-east-2.amazonaws.com/persephone-blocklist"

          port {
            name           = "grpc"
            container_port = 50051
          }

          port {
            name           = "metrics"
            container_port = 7000
          }

          env {
            name  = "SCAMDB_HOST"
            value = "scamdb"
          }

          env {
            name  = "SCAMDB_PORT"
            value = "50051"
          }

          resources {
            limits = {
              cpu = "1"

              memory = "1Gi"
            }

            requests = {
              cpu = "250m"

              memory = "256Mi"
            }
          }

          image_pull_policy = "Always"
        }

        termination_grace_period_seconds = 30
      }
    }
  }
}

resource "kubernetes_deployment" "scamdb" {
  metadata {
    name      = "scamdb"
    namespace = "persephone"

    labels = {
      app = "scamdb"

      team = "persephone"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "scamdb"
      }
    }

    template {
      metadata {
        labels = {
          app = "scamdb"

          team = "persephone"
        }
      }

      spec {
        container {
          name  = "scamdb"
          image = "618394590705.dkr.ecr.us-east-2.amazonaws.com/persephone-scamdb"

          port {
            name           = "grpc"
            container_port = 50051
          }

          port {
            name           = "metrics"
            container_port = 7000
          }

          env {
            name  = "MONGODB_HOST"
            value = "mongodb"
          }

          env {
            name  = "MONGODB_PORT"
            value = "27017"
          }

          resources {
            limits = {
              cpu = "500m"

              memory = "512Mi"
            }

            requests = {
              cpu = "125m"

              memory = "128Mi"
            }
          }

          image_pull_policy = "Always"
        }

        termination_grace_period_seconds = 30
      }
    }
  }
}

resource "kubernetes_deployment" "timeofday" {
  metadata {
    name      = "timeofday"
    namespace = "persephone"

    labels = {
      app = "timeofday"

      team = "persephone"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "timeofday"
      }
    }

    template {
      metadata {
        labels = {
          app = "timeofday"

          team = "persephone"
        }
      }

      spec {
        container {
          name  = "timeofday"
          image = "618394590705.dkr.ecr.us-east-2.amazonaws.com/persephone-timeofday"

          port {
            name           = "grpc"
            container_port = 50051
          }

          port {
            name           = "metrics"
            container_port = 7000
          }

          resources {
            limits = {
              cpu = "500m"

              memory = "512Mi"
            }

            requests = {
              cpu = "125m"

              memory = "128Mi"
            }
          }

          image_pull_policy = "Always"
        }

        termination_grace_period_seconds = 30
      }
    }
  }
}

resource "kubernetes_deployment" "event_generator" {
  metadata {
    name      = "event-generator"
    namespace = "persephone"

    labels = {
      app = "event-generator"

      team = "persephone"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "event-generator"
      }
    }

    template {
      metadata {
        labels = {
          app = "event-generator"

          team = "persephone"
        }
      }

      spec {
        container {
          name  = "event-generator"
          image = "618394590705.dkr.ecr.us-east-2.amazonaws.com/persephone-event-generator"

          env {
            name  = "API_HOST"
            value = "rest"
          }

          env {
            name  = "API_PORT"
            value = "8080"
          }

          resources {
            limits = {
              cpu = "256m"

              memory = "256Mi"
            }

            requests = {
              cpu = "128m"

              memory = "128Mi"
            }
          }

          image_pull_policy = "Always"
        }

        termination_grace_period_seconds = 30
      }
    }
  }
}

