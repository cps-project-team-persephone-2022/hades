import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#071d49',
        secondary: '#df1995',
        orange: '#ff9e1b',
        dblue: '#00a3e0',
        lblue: '#71c5e8',
        yellow: '#fedb00',
        error: '#b71c1c',
        fo: '#071d49',
      },
      dark: {
        secondary: '#df1995',
        orange: '#ff9e1b',
        dblue: '#00a3e0',
        lblue: '#71c5e8',
        yellow: '#fedb00',
        error: '#b71c1c',
        fo: '#071d49',
      },
    },
    dark: false,
  },
});
