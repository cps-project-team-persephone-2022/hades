type ScoringResult = {
  eventId: string;
  event: {
    aNumber: string;
    bNumber: string;
    callTime: number | string;
  };
  blockCall: boolean;
  isScam: boolean;
  score: number;
  disposition?: string;
};

export default ScoringResult;
