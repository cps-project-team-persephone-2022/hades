type NumberInfo = {
  categoryCode: string;
  categoryName: string;
  blockedAnumbers: string[];
  blockedCategories: string[];
};

export default NumberInfo;
