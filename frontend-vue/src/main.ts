import Vue from 'vue';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import vuexStore from './store';

Vue.config.productionTip = false;

new Vue({
  router,
  store: vuexStore,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
