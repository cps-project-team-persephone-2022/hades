import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Landing from '@/views/Landing.vue';
import Scoring from '@/views/Scoring.vue';
import Lookup from '@/views/Lookup.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'landing',
    component: Landing,
  },
  {
    path: '/scoring',
    name: 'scoring',
    component: Scoring,
  },
  {
    path: '/lookup',
    name: 'lookup',
    component: Lookup,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
