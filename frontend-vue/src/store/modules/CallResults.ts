import ScoringResult from '@/types/ScoringResult';
import axios from 'axios';
import { Action, State } from 'vuex-simple';

export default class CallResults {
  @State()
  public results: ScoringResult[] | null = [];

  @State()
  public limit: number = 5;

  @Action()
  public async fetchResults(limit: number, offset: number) {
    try {
      const urlString = `api/call-protection/results?limit=${limit}&offset=${offset}`;
      const res = await axios.get(urlString);
      if (res.status !== 200) throw new Error(res.status.toString());
      this.results = res.data.body as ScoringResult[];
    } catch (e) {}
  }
}
