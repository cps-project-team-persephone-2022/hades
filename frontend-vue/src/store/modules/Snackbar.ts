import { Action, Mutation, State } from 'vuex-simple';

export default class Snackbar {
  @State()
  public snackbar = {
    show: false,
    text: '',
  };

  @Mutation()
  private showSnackbar(text: string) {
    this.snackbar.show = true;
    this.snackbar.text = text;
  }

  @Action()
  public errorMessage(text: string) {
    this.showSnackbar(text);
  }
}
