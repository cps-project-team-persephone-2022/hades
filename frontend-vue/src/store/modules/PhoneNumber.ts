import { Action, Mutation, State } from 'vuex-simple';
import axios from 'axios';
import NumberInfo from '@/types/NumberInfo';
import ScoringResult from '@/types/ScoringResult';

export default class PhoneNumber {
  @State()
  public searchNum: string = '444';

  @State()
  public isValid: boolean = true;

  @State()
  public numberInfo: NumberInfo | null = null;

  @State()
  public scoringResults: ScoringResult[] = [];

  @Mutation()
  private validate() {
    this.isValid = this.searchNum.match(/^\+?[1-9]\d{1,14}$/) !== null;
    return this.isValid;
  }

  @Mutation()
  private async fetchNumberInfo() {
    try {
      const res = await axios.get(`api/call-protection/phone/${this.searchNum}`);
      if (res.status !== 200) throw new Error();
      this.numberInfo = res.data.body as NumberInfo;
    } catch (e) {
      // snackbar error message
    }
  }

  @Mutation()
  private async fetchNumberResults() {
    try {
      const res = await axios.get(`api/call-protection/results/${this.searchNum}`);
      if (res.status !== 200) throw new Error();
      this.scoringResults = res.data.body as ScoringResult[];
    } catch (e) {
      // snackbar error message
    }
  }

  @Action()
  public async getData() {
    if (!this.validate()) {
      // snackbar error message
      return;
    }
    this.fetchNumberInfo();
    this.fetchNumberResults();
  }
}
