import Vue from 'vue';
import Vuex, { Store } from 'vuex';
import Snackbar from '@/store/modules/Snackbar';
import PhoneNumber from '@/store/modules/PhoneNumber';
import CallResults from '@/store/modules/CallResults';
import { createVuexStore, Module, useStore } from 'vuex-simple';

Vue.use(Vuex);

export class RootStore {
  @Module()
  public snackbar: Snackbar = new Snackbar();

  @Module()
  public pNumber: PhoneNumber = new PhoneNumber();

  @Module()
  public callResults: CallResults = new CallResults();
}

const vuexStore: Store<RootStore> = createVuexStore(new RootStore());

export const store = useStore<RootStore>(vuexStore);

export default vuexStore;
