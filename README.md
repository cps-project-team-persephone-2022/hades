# Persephone Call Protection Applications

## Project Structure

- All applications have their own directories. Each `golang` application has its own `Dockerfile` to build container images. The current application directories are:
    - `scamdb` contains the scam database application
    - `rest` contains the rest API
    - `valnum` contains the number validation scam model service
    - `blocklist` contains the blocklist scam model service
    - `timeofday` contains the time of day scam model service
    - `spark` contains the spark data loader
    - `frontend-svelte` contains the svelte-based web GUI for the API service
    - `frontend-vue` contains the vuejs-based web GUI for the API service
    - `event-generator` is a small typescript utility that generates events and sends them to the API on a continuous interval

- Additionally there are directories for `terraform`, `kubernetes`, and `protobuf` files.
    - `kubernetes` stores all the manifests necessary to run the full stack in K8s.
    - `terraform` contains terraform resource representations of the manifests in `kubernetes`
    - `protobuf` contains all the `.proto` files to generate the grpc code.
        - There is a target in the `Makefile` that will compile the protobuf into go code.
        - This can be invoked with `make proto` from the base directory. (Note: the user must have docker installed and running.)
    - `mongoinit` contains the mongo data initialization scripts for local testing.

### `Makefile` and `docker-compose.yml`
- There is a `docker-compose.yml` file that will spin up a local copy of the application for testing. There is a `run` target to do this in the `Makefile` that can be invoked with `make run`
- The `docker` target in the `Makefile` that will build the container images and push them to AWS ECR for use in the remote cluster. This can be invoked with `make docker`
- There is a `vendor` target in the `Makefile` that will build the vendor bundle for each app, making build times much faster for local testing as we can skip the `go mod download` step in the container build stage.

## Data Model
- The mongodb database will have two databases with eponymous collection names.
- `categories` contains the categorizations for particular phone numbers. Format is below.
```json
[
    {
        "phone_number": "000000000",
        "category_code": "code",
        "category_name": "name"
    }
]
```

- `blocklist` contains the blocklist for particular phone numbers. Format is below.
```json
[
    {
        "phone_number" "0000000000",
        "blocked_a_numbers" : [
            "0000000000",
            ...
        ],
        "blocked_categories" : [
            "code",
            ...
        ]
    }
]
```

## Scoring Model
- Our scoring model is very rudimentary. Any score totals `80` or above are considered a possible scam. Any score totals `90` or above are considered "high confidence" and is blocked. The breakdown is as follows.
    - Invalid numbers are scored `40` from the `valnum` service. Additionally if `valnum` detects that a number is invalid, it will return `true` for `IsScam`
    - Any `aNumber` that is categorized as `SC` or `Known Scam` in the database will be scored `100` by the `blocklist` service. Additionally, `blocklist` will return `true` for `BlockCall` if the `bNumber` has the `aNumber` as a blocked number or if `aNumber` is categorized as a category that `bNumber` has explicitly blocked. Any `aNumber` that is not in the database will be considered `UN` or `Unknown` and will be scored a `40`
    - The `timeofday` service determines the likelihood of a scam based on the time of day. If a call happens between 7am and 5pm, it is scored `0` by the`timeofday` service. From 5pm to 9pm it's scored a `20`. From 9pm to 7am, it's scored a `40`. Because it never scores any higher than a `40`, `timeofday` will never alone score a call high enough to consider it a scam.
- The `rest` service calculates the aggregate score from all gRPC services and modifies the `isScam` and `blockCall` appropriately. Additionally, `rest` returns the results from each of these services for additional context.

## API Model
The following endpoints and data shapes are exposed via the API under `/api/call-protection/`:

- `calls` is a `POST` endpoint accepting a single call event. It returns a `score` shaped like below:
```json
{
	"body": {
		"eventId": UUID,
		"isScam": Boolean,
		"blockCall": Boolean,
		"metadata": {
			"blocklistDecision": String,
			"category": String,
			"hourOfDay": Int,
			"numvalDecision": String,
			"score": Int
		}
	}
}
```
- `batch` is a `POST` endpoint accepting an array of call events. It returns an array of the `score` objects above.

- `phone` is an endpoint that accepts `GET` requests with a phone number as a path parameter. It returns an `info` object shaped like below:
```json
{
	"body": {
		"categoryCode": String,
		"categoryName": String,
		"blockedAnumbers": [
            ...String
		],
		"blockedCategories": [
            ...String
        ]
	}
}
```
- `phone` also accepts `PUT` requests. `PUT` request bodies must be fully formed `info` objects shaped the same as would be returned by a `GET` request. The response for a successful `PUT` is a `204` status code.

## Requests
- The `requests.json` file contains test requests for use with the initialized database. As JSON does now allow for comments in code, the the result of these requests should be as follows, in order from top of file to bottom of file:
    1. Complete.  Call made from valid category `aNumber` to `bNumber` with no information in database.
    2. Reject.  Call made from known scam `aNumber`.  Personal blocklist is empty for the `bNumber`.
    3. Reject.  Call made from known scam `aNumber`.  Personal blocklist is not empty for the `bNumber`.
    4. Reject.  Call made from valid category `aNumber`, but `bNumber` is blocking that category.
    5. Reject.  Call made from unknown category `aNumber`, but `bNumber` has `aNumber` on personal blocklist.
    6. Complete.  Call made from valid category `aNumber` to `bNumber` with blocklist, but `aNumber` nor its category are being blocked.
