run: | clean
	docker-compose up --build -d

clean:
	docker-compose down --remove-orphans

vendor: | proto
	for module in rest scamdb timeofday valnum blocklist; \
	do \
		cd $$module && go mod tidy && go mod vendor && cd -; \
	done

proto:
	docker run --rm --platform linux/amd64 -v "`pwd`/protobuf:/protobuf" -v "`pwd`/scamdb/scamdbpb:/targets/scamdbpb" -w / zcking/protoc-go:3.17.3 protoc -I protobuf --go_out=targets/scamdbpb --go-grpc_out=targets/scamdbpb --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative protobuf/scamdb.proto
	docker run --rm --platform linux/amd64 -v "`pwd`/protobuf:/protobuf" -v "`pwd`/blocklist/scamdbpb:/targets/scamdbpb:" -w / zcking/protoc-go:3.17.3 protoc -I protobuf --go_out=targets/scamdbpb --go-grpc_out=targets/scamdbpb --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative protobuf/scamdb.proto
	docker run --rm --platform linux/amd64 -v "`pwd`/protobuf:/protobuf" -v "`pwd`/rest/scamdbpb:/targets/scamdbpb:" -w / zcking/protoc-go:3.17.3 protoc -I protobuf --go_out=targets/scamdbpb --go-grpc_out=targets/scamdbpb --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative protobuf/scamdb.proto
	docker run --rm --platform linux/amd64 -v "`pwd`/protobuf:/protobuf" -v "`pwd`/valnum/scammodelpb:/targets/scammodelpb" -w / zcking/protoc-go:3.17.3 protoc -I protobuf --go_out=targets/scammodelpb --go-grpc_out=targets/scammodelpb --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative protobuf/scammodel.proto
	docker run --rm --platform linux/amd64 -v "`pwd`/protobuf:/protobuf" -v "`pwd`/rest/scammodelpb:/targets/scammodelpb" -w / zcking/protoc-go:3.17.3 protoc -I protobuf --go_out=targets/scammodelpb --go-grpc_out=targets/scammodelpb --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative protobuf/scammodel.proto
	docker run --rm --platform linux/amd64 -v "`pwd`/protobuf:/protobuf" -v "`pwd`/blocklist/scammodelpb:/targets/scammodelpb" -w / zcking/protoc-go:3.17.3 protoc -I protobuf --go_out=targets/scammodelpb --go-grpc_out=targets/scammodelpb --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative protobuf/scammodel.proto
	docker run --rm --platform linux/amd64 -v "`pwd`/protobuf:/protobuf" -v "`pwd`/timeofday/scammodelpb:/targets/scammodelpb" -w / zcking/protoc-go:3.17.3 protoc -I protobuf --go_out=targets/scammodelpb --go-grpc_out=targets/scammodelpb --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative protobuf/scammodel.proto

vue:
	cd frontend-vue && pnpm install && pnpm run build && cd -

deploy: | vue docker
	export AWS_PROFILE=618394590705 && cd terraform && terraform apply -auto-approve && cd -
	aws s3 sync frontend-vue/dist s3://$$(terraform -chdir=terraform output -raw s3_bucket)

docker: | vendor vue
	export AWS_PROFILE=618394590705 && aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 618394590705.dkr.ecr.us-east-2.amazonaws.com
	docker buildx bake --push --set *.platform=linux/amd64,linux/arm64/v8 -f docker-compose.yml