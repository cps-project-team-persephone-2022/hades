import { randomInt } from 'crypto';
import http from 'http';
import log4js from 'log4js';

const logger = log4js.getLogger();
logger.level = log4js.levels.INFO;

type Request = {
  eventId: string;
  aNumber: string;
  bNumber: string;
  sipInvite: string;
};

const API_HOST = process.env.API_HOST || 'localhost';
const API_PORT = process.env.API_PORT || 8080;

const genUUID = () => {
  const RFC4122_TEMPLATE = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
  const replacePlaceholders = (placeholder: string) => {
    const random = Math.floor(Math.random() * 16) % 16;
    const value = placeholder === 'x' ? random : (random & 0x3) | 0x8;
    return value.toString(16);
  };
  return RFC4122_TEMPLATE.replace(/[xy]/g, replacePlaceholders);
};

const genPhoneNumber = () => {
  return (Math.floor(Math.random() * 10000000000) % 10000000000).toString();
};

const sendBatch = (data: Object) => {
  const options = {
    hostname: API_HOST,
    port: API_PORT,
    path: '/api/call-protection/batch',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const req = http.request(options, (res) => logger.info(`Status: ${res.statusCode}`));
  req.write(JSON.stringify(data));
};

const genRequests = () => {
  let iter = randomInt(10);
  let requests: Request[] = [];
  for (let i = 0; i < iter; i++) {
    let req: Request = {
      eventId: genUUID(),
      aNumber: genPhoneNumber(),
      bNumber: genPhoneNumber(),
      sipInvite: 'BLAH BLAH BLAH',
    };
    requests.push(req);
  }
  return requests;
};

const intervalSend = () => {
  let payload = genRequests();
  sendBatch(payload);
  logger.info(`Sending batch of ${payload.length} ${payload.length === 1 ? 'event' : 'events'}...`);
};

intervalSend();
setInterval(() => intervalSend(), 60000);
