"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = require("crypto");
const http_1 = __importDefault(require("http"));
const log4js_1 = __importDefault(require("log4js"));
const logger = log4js_1.default.getLogger();
logger.level = log4js_1.default.levels.INFO;
const API_HOST = process.env.API_HOST || 'localhost';
const API_PORT = process.env.API_PORT || 8080;
const genUUID = () => {
    const RFC4122_TEMPLATE = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
    const replacePlaceholders = (placeholder) => {
        const random = Math.floor(Math.random() * 16) % 16;
        const value = placeholder === 'x' ? random : (random & 0x3) | 0x8;
        return value.toString(16);
    };
    return RFC4122_TEMPLATE.replace(/[xy]/g, replacePlaceholders);
};
const genPhoneNumber = () => {
    return (Math.floor(Math.random() * 10000000000) % 10000000000).toString();
};
const sendBatch = (data) => {
    const options = {
        hostname: API_HOST,
        port: API_PORT,
        path: '/api/call-protection/batch',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const req = http_1.default.request(options, (res) => logger.info(`Status: ${res.statusCode}`));
    req.write(JSON.stringify(data));
};
const genRequests = () => {
    let iter = (0, crypto_1.randomInt)(10);
    let requests = [];
    for (let i = 0; i < iter; i++) {
        let req = {
            eventId: genUUID(),
            aNumber: genPhoneNumber(),
            bNumber: genPhoneNumber(),
            sipInvite: 'BLAH BLAH BLAH',
        };
        requests.push(req);
    }
    return requests;
};
setInterval(() => {
    let payload = genRequests();
    sendBatch(payload);
    logger.info(`Sending batch of ${payload.length} ${payload.length === 1 ? 'event' : 'events'}...`);
}, 10000);
