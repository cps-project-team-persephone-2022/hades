package main

import (
	"context"
	"fmt"
	"io"
	"net/http"

	"google.golang.org/protobuf/types/known/structpb"

	"log"
	"net"
	"valnum/scammodelpb"

	"google.golang.org/grpc/reflection"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/ttacon/libphonenumber"
	"google.golang.org/grpc"
)

type server struct {
	scammodelpb.ScamServiceServer
}

func main() {
	ascii()
	log.Println("Staring ValidNumber ScamModel server...")
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	s := grpc.NewServer()
	scammodelpb.RegisterScamServiceServer(s, &server{})
	reflection.Register(s)

	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(":7000", nil)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}

func validateNumber(number string) (reason string, score int) {
	parsed, err := libphonenumber.Parse(number, "US")
	if err != nil {
		log.Printf("Failed to parse number: %v", err)
		return "Number could not be parsed", 40
	}
	verdict := libphonenumber.IsPossibleNumberWithReason(parsed)
	switch verdict {
	case libphonenumber.IS_POSSIBLE:
		return "Valid Number", 0
	case libphonenumber.INVALID_COUNTRY_CODE:
		return "Invalid Country Code", 80
	case libphonenumber.TOO_SHORT:
		return "Number too short", 80
	case libphonenumber.TOO_LONG:
		return "Number too long", 80
	default: // Need this case for the compiler. should never happen
		return "I'm a teapot", 100
	}
}

func buildResponse(req *scammodelpb.ScoreEventRequest) *scammodelpb.ScoreEventResponse {
	var meta *structpb.Struct
	log.Printf("Scoring event: %v", req)
	reason, score := validateNumber(req.ANumber)
	meta, _ = structpb.NewStruct(map[string]interface{}{
		"numvalDecision": reason,
	})
	res := &scammodelpb.ScoreEventResponse{
		EventId:   req.GetEventId(),
		BlockCall: false,
		IsScam:    score > 10,
		Score:     int64(score),
		Metadata:  meta,
	}
	return res
}

func (*server) ScoreEvent(ctx context.Context, req *scammodelpb.ScoreEventRequest) (*scammodelpb.ScoreEventResponse, error) {
	log.Println("Received an event to score...")
	res := buildResponse(req)
	return res, nil
}

func (*server) ScoreEvents(stream scammodelpb.ScamService_ScoreEventsServer) error {
	log.Println("Received a batch of events to score...")
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			log.Printf("Failed to read client stream: %v", err)
			return err
		}
		res := buildResponse(req)
		err = stream.Send(res)
		if err != nil {
			log.Printf("Failed to send client stream: %v", err)
			return err
		}
	}
}

func ascii() {
	fmt.Printf(" ▄ .▄ ▄▄▄· ·▄▄▄▄  ▄▄▄ ..▄▄ · \n██▪▐█▐█ ▀█ ██▪ ██ ▀▄.▀·▐█ ▀. \n██▀▐█▄█▀▀█ ▐█· ▐█▌▐▀▀▪▄▄▀▀▀█▄\n██▌▐▀▐█ ▪▐▌██. ██ ▐█▄▄▌▐█▄▪▐█\n▀▀▀ · ▀  ▀ ▀▀▀▀▀•  ▀▀▀  ▀▀▀▀\n")
}
