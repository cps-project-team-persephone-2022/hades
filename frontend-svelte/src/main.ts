import Persephone from './Persephone.svelte';

const app = new Persephone({
  target: document.body,
});

export default app;
