
(function(l, r) { if (!l || l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (self.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(self.document);
var app = (function () {
    'use strict';

    function noop() { }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function is_empty(obj) {
        return Object.keys(obj).length === 0;
    }
    function null_to_empty(value) {
        return value == null ? '' : value;
    }
    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function prevent_default(fn) {
        return function (event) {
            event.preventDefault();
            // @ts-ignore
            return fn.call(this, event);
        };
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_input_value(input, value) {
        input.value = value == null ? '' : value;
    }
    function custom_event(type, detail, bubbles = false) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, bubbles, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    // flush() calls callbacks in this order:
    // 1. All beforeUpdate callbacks, in order: parents before children
    // 2. All bind:this callbacks, in reverse order: children before parents.
    // 3. All afterUpdate callbacks, in order: parents before children. EXCEPT
    //    for afterUpdates called during the initial onMount, which are called in
    //    reverse order: children before parents.
    // Since callbacks might update component values, which could trigger another
    // call to flush(), the following steps guard against this:
    // 1. During beforeUpdate, any updated components will be added to the
    //    dirty_components array and will cause a reentrant call to flush(). Because
    //    the flush index is kept outside the function, the reentrant call will pick
    //    up where the earlier call left off and go through all dirty components. The
    //    current_component value is saved and restored so that the reentrant call will
    //    not interfere with the "parent" flush() call.
    // 2. bind:this callbacks cannot trigger new flush() calls.
    // 3. During afterUpdate, any updated components will NOT have their afterUpdate
    //    callback called a second time; the seen_callbacks set, outside the flush()
    //    function, guarantees this behavior.
    const seen_callbacks = new Set();
    let flushidx = 0; // Do *not* move this inside the flush() function
    function flush() {
        const saved_component = current_component;
        do {
            // first, call beforeUpdate functions
            // and update components
            while (flushidx < dirty_components.length) {
                const component = dirty_components[flushidx];
                flushidx++;
                set_current_component(component);
                update(component.$$);
            }
            set_current_component(null);
            dirty_components.length = 0;
            flushidx = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        seen_callbacks.clear();
        set_current_component(saved_component);
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function mount_component(component, target, anchor, customElement) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        if (!customElement) {
            // onMount happens before the initial afterUpdate
            add_render_callback(() => {
                const new_on_destroy = on_mount.map(run).filter(is_function);
                if (on_destroy) {
                    on_destroy.push(...new_on_destroy);
                }
                else {
                    // Edge case - component was destroyed immediately,
                    // most likely as a result of a binding initialising
                    run_all(new_on_destroy);
                }
                component.$$.on_mount = [];
            });
        }
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, append_styles, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            on_disconnect: [],
            before_update: [],
            after_update: [],
            context: new Map(options.context || (parent_component ? parent_component.$$.context : [])),
            // everything else
            callbacks: blank_object(),
            dirty,
            skip_bound: false,
            root: options.target || parent_component.$$.root
        };
        append_styles && append_styles($$.root);
        let ready = false;
        $$.ctx = instance
            ? instance(component, options.props || {}, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if (!$$.skip_bound && $$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor, options.customElement);
            flush();
        }
        set_current_component(parent_component);
    }
    /**
     * Base class for Svelte components. Used when dev=false.
     */
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set($$props) {
            if (this.$$set && !is_empty($$props)) {
                this.$$.skip_bound = true;
                this.$$set($$props);
                this.$$.skip_bound = false;
            }
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.46.4' }, detail), true));
    }
    function append_dev(target, node) {
        dispatch_dev('SvelteDOMInsert', { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev('SvelteDOMInsert', { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev('SvelteDOMRemove', { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ['capture'] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev('SvelteDOMAddEventListener', { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev('SvelteDOMRemoveEventListener', { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev('SvelteDOMRemoveAttribute', { node, attribute });
        else
            dispatch_dev('SvelteDOMSetAttribute', { node, attribute, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.wholeText === data)
            return;
        dispatch_dev('SvelteDOMSetData', { node: text, data });
        text.data = data;
    }
    function validate_each_argument(arg) {
        if (typeof arg !== 'string' && !(arg && typeof arg === 'object' && 'length' in arg)) {
            let msg = '{#each} only iterates over array-like objects.';
            if (typeof Symbol === 'function' && arg && Symbol.iterator in arg) {
                msg += ' You can use a spread to convert this iterable into an array.';
            }
            throw new Error(msg);
        }
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    /**
     * Base class for Svelte components with some minor dev-enhancements. Used when dev=true.
     */
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error("'target' is a required option");
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn('Component was already destroyed'); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    /* src/Persephone.svelte generated by Svelte v3.46.4 */

    const file = "src/Persephone.svelte";

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[16] = list[i];
    	return child_ctx;
    }

    function get_each_context_1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[19] = list[i];
    	return child_ctx;
    }

    // (83:6) {#if requests.length > 0}
    function create_if_block_3(ctx) {
    	let each_1_anchor;
    	let each_value_1 = /*requests*/ ctx[1];
    	validate_each_argument(each_value_1);
    	let each_blocks = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks[i] = create_each_block_1(get_each_context_1(ctx, each_value_1, i));
    	}

    	const block = {
    		c: function create() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert_dev(target, each_1_anchor, anchor);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*requests*/ 2) {
    				each_value_1 = /*requests*/ ctx[1];
    				validate_each_argument(each_value_1);
    				let i;

    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1(ctx, each_value_1, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_1.length;
    			}
    		},
    		d: function destroy(detaching) {
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(each_1_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_3.name,
    		type: "if",
    		source: "(83:6) {#if requests.length > 0}",
    		ctx
    	});

    	return block;
    }

    // (84:8) {#each requests as req}
    function create_each_block_1(ctx) {
    	let div;
    	let h3;
    	let t0;
    	let t1_value = /*req*/ ctx[19].eventId + "";
    	let t1;
    	let t2;
    	let p0;
    	let t3;
    	let t4_value = /*req*/ ctx[19].aNumber + "";
    	let t4;
    	let t5;
    	let p1;
    	let t6;
    	let t7_value = /*req*/ ctx[19].bNumber + "";
    	let t7;
    	let t8;

    	const block = {
    		c: function create() {
    			div = element("div");
    			h3 = element("h3");
    			t0 = text("Event ID: ");
    			t1 = text(t1_value);
    			t2 = space();
    			p0 = element("p");
    			t3 = text("Calling Number: ");
    			t4 = text(t4_value);
    			t5 = space();
    			p1 = element("p");
    			t6 = text("Receiving Number: ");
    			t7 = text(t7_value);
    			t8 = space();
    			attr_dev(h3, "class", "svelte-1salg1v");
    			add_location(h3, file, 85, 12, 2767);
    			attr_dev(p0, "class", "svelte-1salg1v");
    			add_location(p0, file, 86, 12, 2812);
    			attr_dev(p1, "class", "svelte-1salg1v");
    			add_location(p1, file, 87, 12, 2861);
    			attr_dev(div, "class", "svelte-1salg1v");
    			add_location(div, file, 84, 10, 2749);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, h3);
    			append_dev(h3, t0);
    			append_dev(h3, t1);
    			append_dev(div, t2);
    			append_dev(div, p0);
    			append_dev(p0, t3);
    			append_dev(p0, t4);
    			append_dev(div, t5);
    			append_dev(div, p1);
    			append_dev(p1, t6);
    			append_dev(p1, t7);
    			append_dev(div, t8);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*requests*/ 2 && t1_value !== (t1_value = /*req*/ ctx[19].eventId + "")) set_data_dev(t1, t1_value);
    			if (dirty & /*requests*/ 2 && t4_value !== (t4_value = /*req*/ ctx[19].aNumber + "")) set_data_dev(t4, t4_value);
    			if (dirty & /*requests*/ 2 && t7_value !== (t7_value = /*req*/ ctx[19].bNumber + "")) set_data_dev(t7, t7_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_1.name,
    		type: "each",
    		source: "(84:8) {#each requests as req}",
    		ctx
    	});

    	return block;
    }

    // (116:6) {:else}
    function create_else_block_1(ctx) {
    	let p;

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "No results yet...";
    			attr_dev(p, "class", "svelte-1salg1v");
    			add_location(p, file, 116, 8, 4001);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block_1.name,
    		type: "else",
    		source: "(116:6) {:else}",
    		ctx
    	});

    	return block;
    }

    // (94:6) {#if responses.length > 0 && responses}
    function create_if_block(ctx) {
    	let each_1_anchor;
    	let each_value = /*responses*/ ctx[2];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert_dev(target, each_1_anchor, anchor);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*responses*/ 4) {
    				each_value = /*responses*/ ctx[2];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		d: function destroy(detaching) {
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(each_1_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(94:6) {#if responses.length > 0 && responses}",
    		ctx
    	});

    	return block;
    }

    // (108:12) {:else}
    function create_else_block(ctx) {
    	let p0;
    	let t1;
    	let p1;
    	let t2_value = /*res*/ ctx[16].metadata.numvalDecision + "";
    	let t2;
    	let t3;
    	let p2;
    	let t4_value = /*res*/ ctx[16].metadata.category + "";
    	let t4;
    	let t5;
    	let p3;
    	let t6;
    	let t7_value = /*res*/ ctx[16].metadata.score + "";
    	let t7;

    	const block = {
    		c: function create() {
    			p0 = element("p");
    			p0.textContent = "Valid";
    			t1 = space();
    			p1 = element("p");
    			t2 = text(t2_value);
    			t3 = space();
    			p2 = element("p");
    			t4 = text(t4_value);
    			t5 = space();
    			p3 = element("p");
    			t6 = text("Score: ");
    			t7 = text(t7_value);
    			attr_dev(p0, "class", "svelte-1salg1v");
    			add_location(p0, file, 108, 14, 3725);
    			attr_dev(p1, "class", "valid svelte-1salg1v");
    			add_location(p1, file, 109, 14, 3752);
    			attr_dev(p2, "class", "category svelte-1salg1v");
    			add_location(p2, file, 110, 14, 3817);
    			attr_dev(p3, "class", "score svelte-1salg1v");
    			add_location(p3, file, 111, 14, 3879);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p0, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, p1, anchor);
    			append_dev(p1, t2);
    			insert_dev(target, t3, anchor);
    			insert_dev(target, p2, anchor);
    			append_dev(p2, t4);
    			insert_dev(target, t5, anchor);
    			insert_dev(target, p3, anchor);
    			append_dev(p3, t6);
    			append_dev(p3, t7);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*responses*/ 4 && t2_value !== (t2_value = /*res*/ ctx[16].metadata.numvalDecision + "")) set_data_dev(t2, t2_value);
    			if (dirty & /*responses*/ 4 && t4_value !== (t4_value = /*res*/ ctx[16].metadata.category + "")) set_data_dev(t4, t4_value);
    			if (dirty & /*responses*/ 4 && t7_value !== (t7_value = /*res*/ ctx[16].metadata.score + "")) set_data_dev(t7, t7_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p0);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(p1);
    			if (detaching) detach_dev(t3);
    			if (detaching) detach_dev(p2);
    			if (detaching) detach_dev(t5);
    			if (detaching) detach_dev(p3);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(108:12) {:else}",
    		ctx
    	});

    	return block;
    }

    // (103:33) 
    function create_if_block_2(ctx) {
    	let p0;
    	let t1;
    	let p1;
    	let t2_value = /*res*/ ctx[16].metadata.numvalDecision + "";
    	let t2;
    	let t3;
    	let p2;
    	let t4_value = /*res*/ ctx[16].metadata.category + "";
    	let t4;
    	let t5;
    	let p3;
    	let t6;
    	let t7_value = /*res*/ ctx[16].metadata.score + "";
    	let t7;

    	const block = {
    		c: function create() {
    			p0 = element("p");
    			p0.textContent = "Scam Likely";
    			t1 = space();
    			p1 = element("p");
    			t2 = text(t2_value);
    			t3 = space();
    			p2 = element("p");
    			t4 = text(t4_value);
    			t5 = space();
    			p3 = element("p");
    			t6 = text("Score: ");
    			t7 = text(t7_value);
    			attr_dev(p0, "class", "svelte-1salg1v");
    			add_location(p0, file, 103, 14, 3482);
    			attr_dev(p1, "class", "valid svelte-1salg1v");
    			add_location(p1, file, 104, 14, 3515);
    			attr_dev(p2, "class", "category svelte-1salg1v");
    			add_location(p2, file, 105, 14, 3580);
    			attr_dev(p3, "class", "score svelte-1salg1v");
    			add_location(p3, file, 106, 14, 3642);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p0, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, p1, anchor);
    			append_dev(p1, t2);
    			insert_dev(target, t3, anchor);
    			insert_dev(target, p2, anchor);
    			append_dev(p2, t4);
    			insert_dev(target, t5, anchor);
    			insert_dev(target, p3, anchor);
    			append_dev(p3, t6);
    			append_dev(p3, t7);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*responses*/ 4 && t2_value !== (t2_value = /*res*/ ctx[16].metadata.numvalDecision + "")) set_data_dev(t2, t2_value);
    			if (dirty & /*responses*/ 4 && t4_value !== (t4_value = /*res*/ ctx[16].metadata.category + "")) set_data_dev(t4, t4_value);
    			if (dirty & /*responses*/ 4 && t7_value !== (t7_value = /*res*/ ctx[16].metadata.score + "")) set_data_dev(t7, t7_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p0);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(p1);
    			if (detaching) detach_dev(t3);
    			if (detaching) detach_dev(p2);
    			if (detaching) detach_dev(t5);
    			if (detaching) detach_dev(p3);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2.name,
    		type: "if",
    		source: "(103:33) ",
    		ctx
    	});

    	return block;
    }

    // (98:12) {#if res.blockCall}
    function create_if_block_1(ctx) {
    	let p0;
    	let t1;
    	let p1;
    	let t2_value = /*res*/ ctx[16].metadata.numvalDecision + "";
    	let t2;
    	let t3;
    	let p2;
    	let t4_value = /*res*/ ctx[16].metadata.category + "";
    	let t4;
    	let t5;
    	let p3;
    	let t6;
    	let t7_value = /*res*/ ctx[16].metadata.score + "";
    	let t7;

    	const block = {
    		c: function create() {
    			p0 = element("p");
    			p0.textContent = "Blocked";
    			t1 = space();
    			p1 = element("p");
    			t2 = text(t2_value);
    			t3 = space();
    			p2 = element("p");
    			t4 = text(t4_value);
    			t5 = space();
    			p3 = element("p");
    			t6 = text("Score: ");
    			t7 = text(t7_value);
    			attr_dev(p0, "class", "svelte-1salg1v");
    			add_location(p0, file, 98, 14, 3229);
    			attr_dev(p1, "class", "valid svelte-1salg1v");
    			add_location(p1, file, 99, 14, 3258);
    			attr_dev(p2, "class", "category svelte-1salg1v");
    			add_location(p2, file, 100, 14, 3323);
    			attr_dev(p3, "class", "score svelte-1salg1v");
    			add_location(p3, file, 101, 14, 3385);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p0, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, p1, anchor);
    			append_dev(p1, t2);
    			insert_dev(target, t3, anchor);
    			insert_dev(target, p2, anchor);
    			append_dev(p2, t4);
    			insert_dev(target, t5, anchor);
    			insert_dev(target, p3, anchor);
    			append_dev(p3, t6);
    			append_dev(p3, t7);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*responses*/ 4 && t2_value !== (t2_value = /*res*/ ctx[16].metadata.numvalDecision + "")) set_data_dev(t2, t2_value);
    			if (dirty & /*responses*/ 4 && t4_value !== (t4_value = /*res*/ ctx[16].metadata.category + "")) set_data_dev(t4, t4_value);
    			if (dirty & /*responses*/ 4 && t7_value !== (t7_value = /*res*/ ctx[16].metadata.score + "")) set_data_dev(t7, t7_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p0);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(p1);
    			if (detaching) detach_dev(t3);
    			if (detaching) detach_dev(p2);
    			if (detaching) detach_dev(t5);
    			if (detaching) detach_dev(p3);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(98:12) {#if res.blockCall}",
    		ctx
    	});

    	return block;
    }

    // (95:8) {#each responses as res}
    function create_each_block(ctx) {
    	let div;
    	let h3;
    	let t0_value = /*res*/ ctx[16].eventId + "";
    	let t0;
    	let t1;
    	let t2;
    	let div_class_value;

    	function select_block_type_1(ctx, dirty) {
    		if (/*res*/ ctx[16].blockCall) return create_if_block_1;
    		if (/*res*/ ctx[16].isScam) return create_if_block_2;
    		return create_else_block;
    	}

    	let current_block_type = select_block_type_1(ctx);
    	let if_block = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			div = element("div");
    			h3 = element("h3");
    			t0 = text(t0_value);
    			t1 = space();
    			if_block.c();
    			t2 = space();
    			attr_dev(h3, "class", "svelte-1salg1v");
    			add_location(h3, file, 96, 12, 3160);

    			attr_dev(div, "class", div_class_value = "" + (null_to_empty(/*res*/ ctx[16].blockCall
    			? 'blocked'
    			: /*res*/ ctx[16].isScam ? 'scam' : '') + " svelte-1salg1v"));

    			add_location(div, file, 95, 10, 3081);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, h3);
    			append_dev(h3, t0);
    			append_dev(div, t1);
    			if_block.m(div, null);
    			append_dev(div, t2);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*responses*/ 4 && t0_value !== (t0_value = /*res*/ ctx[16].eventId + "")) set_data_dev(t0, t0_value);

    			if (current_block_type === (current_block_type = select_block_type_1(ctx)) && if_block) {
    				if_block.p(ctx, dirty);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(div, t2);
    				}
    			}

    			if (dirty & /*responses*/ 4 && div_class_value !== (div_class_value = "" + (null_to_empty(/*res*/ ctx[16].blockCall
    			? 'blocked'
    			: /*res*/ ctx[16].isScam ? 'scam' : '') + " svelte-1salg1v"))) {
    				attr_dev(div, "class", div_class_value);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if_block.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(95:8) {#each responses as res}",
    		ctx
    	});

    	return block;
    }

    function create_fragment(ctx) {
    	let main;
    	let header;
    	let h1;
    	let t1;
    	let a0;
    	let t3;
    	let article;
    	let form;
    	let fieldset0;
    	let button0;
    	let t5;
    	let button1;
    	let t7;
    	let button2;
    	let t9;
    	let button3;
    	let t11;
    	let label0;
    	let t13;
    	let input0;
    	let t14;
    	let label1;
    	let t16;
    	let input1;
    	let t17;
    	let label2;
    	let t19;
    	let input2;
    	let t20;
    	let textarea;
    	let t21;
    	let section0;
    	let fieldset1;
    	let button4;
    	let t23;
    	let t24;
    	let section1;
    	let t25;
    	let footer;
    	let t26;
    	let a1;
    	let mounted;
    	let dispose;
    	let if_block0 = /*requests*/ ctx[1].length > 0 && create_if_block_3(ctx);

    	function select_block_type(ctx, dirty) {
    		if (/*responses*/ ctx[2].length > 0 && /*responses*/ ctx[2]) return create_if_block;
    		return create_else_block_1;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block1 = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			main = element("main");
    			header = element("header");
    			h1 = element("h1");
    			h1.textContent = "Persephone";
    			t1 = space();
    			a0 = element("a");
    			a0.textContent = "BitBucket";
    			t3 = space();
    			article = element("article");
    			form = element("form");
    			fieldset0 = element("fieldset");
    			button0 = element("button");
    			button0.textContent = "Add to batch";
    			t5 = space();
    			button1 = element("button");
    			button1.textContent = "Add 10";
    			t7 = space();
    			button2 = element("button");
    			button2.textContent = "Add 100";
    			t9 = space();
    			button3 = element("button");
    			button3.textContent = "Add 1000";
    			t11 = space();
    			label0 = element("label");
    			label0.textContent = "Event ID";
    			t13 = space();
    			input0 = element("input");
    			t14 = space();
    			label1 = element("label");
    			label1.textContent = "Calling Phone Number";
    			t16 = space();
    			input1 = element("input");
    			t17 = space();
    			label2 = element("label");
    			label2.textContent = "Receiving Phone Number";
    			t19 = space();
    			input2 = element("input");
    			t20 = space();
    			textarea = element("textarea");
    			t21 = space();
    			section0 = element("section");
    			fieldset1 = element("fieldset");
    			button4 = element("button");
    			button4.textContent = "Submit batch";
    			t23 = space();
    			if (if_block0) if_block0.c();
    			t24 = space();
    			section1 = element("section");
    			if_block1.c();
    			t25 = space();
    			footer = element("footer");
    			t26 = text("Copyright © 2022 ");
    			a1 = element("a");
    			a1.textContent = "First Orion";
    			attr_dev(h1, "class", "svelte-1salg1v");
    			add_location(h1, file, 59, 4, 1555);
    			attr_dev(a0, "href", "https://bitbucket.org/cps-project-team-persephone-2022/hades/src/main/");
    			attr_dev(a0, "target", "_blank");
    			attr_dev(a0, "class", "svelte-1salg1v");
    			add_location(a0, file, 60, 4, 1579);
    			attr_dev(header, "class", "svelte-1salg1v");
    			add_location(header, file, 58, 2, 1542);
    			attr_dev(button0, "class", "svelte-1salg1v");
    			add_location(button0, file, 65, 8, 1761);
    			attr_dev(button1, "class", "svelte-1salg1v");
    			add_location(button1, file, 66, 8, 1844);
    			attr_dev(button2, "class", "svelte-1salg1v");
    			add_location(button2, file, 67, 8, 1923);
    			attr_dev(button3, "class", "svelte-1salg1v");
    			add_location(button3, file, 68, 8, 2004);
    			attr_dev(fieldset0, "class", "svelte-1salg1v");
    			add_location(fieldset0, file, 64, 6, 1742);
    			attr_dev(label0, "for", "eventId");
    			attr_dev(label0, "class", "svelte-1salg1v");
    			add_location(label0, file, 70, 6, 2103);
    			attr_dev(input0, "type", "text");
    			attr_dev(input0, "name", "eventId");
    			attr_dev(input0, "class", "svelte-1salg1v");
    			add_location(input0, file, 71, 6, 2147);
    			attr_dev(label1, "for", "aNumber");
    			attr_dev(label1, "class", "svelte-1salg1v");
    			add_location(label1, file, 72, 6, 2221);
    			attr_dev(input1, "type", "text");
    			attr_dev(input1, "name", "aNumber");
    			attr_dev(input1, "class", "svelte-1salg1v");
    			add_location(input1, file, 73, 6, 2277);
    			attr_dev(label2, "for", "bNumber");
    			attr_dev(label2, "class", "svelte-1salg1v");
    			add_location(label2, file, 74, 6, 2351);
    			attr_dev(input2, "type", "text");
    			attr_dev(input2, "name", "bNumber");
    			attr_dev(input2, "class", "svelte-1salg1v");
    			add_location(input2, file, 75, 6, 2409);
    			attr_dev(textarea, "name", "sipInvite");
    			textarea.hidden = true;
    			add_location(textarea, file, 76, 6, 2483);
    			attr_dev(form, "id", "event");
    			attr_dev(form, "class", "svelte-1salg1v");
    			add_location(form, file, 63, 4, 1718);
    			attr_dev(button4, "class", "svelte-1salg1v");
    			add_location(button4, file, 80, 8, 2604);
    			attr_dev(fieldset1, "class", "svelte-1salg1v");
    			add_location(fieldset1, file, 79, 6, 2585);
    			attr_dev(section0, "class", "svelte-1salg1v");
    			add_location(section0, file, 78, 4, 2569);
    			attr_dev(section1, "class", "responses svelte-1salg1v");
    			add_location(section1, file, 92, 4, 2964);
    			attr_dev(article, "class", "svelte-1salg1v");
    			add_location(article, file, 62, 2, 1704);
    			attr_dev(a1, "href", "https://firstorion.com/");
    			attr_dev(a1, "target", "_blank");
    			add_location(a1, file, 121, 26, 4103);
    			attr_dev(footer, "class", "svelte-1salg1v");
    			add_location(footer, file, 120, 2, 4068);
    			attr_dev(main, "class", "svelte-1salg1v");
    			add_location(main, file, 57, 0, 1533);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);
    			append_dev(main, header);
    			append_dev(header, h1);
    			append_dev(header, t1);
    			append_dev(header, a0);
    			append_dev(main, t3);
    			append_dev(main, article);
    			append_dev(article, form);
    			append_dev(form, fieldset0);
    			append_dev(fieldset0, button0);
    			append_dev(fieldset0, t5);
    			append_dev(fieldset0, button1);
    			append_dev(fieldset0, t7);
    			append_dev(fieldset0, button2);
    			append_dev(fieldset0, t9);
    			append_dev(fieldset0, button3);
    			append_dev(form, t11);
    			append_dev(form, label0);
    			append_dev(form, t13);
    			append_dev(form, input0);
    			set_input_value(input0, /*currEvent*/ ctx[0].eventId);
    			append_dev(form, t14);
    			append_dev(form, label1);
    			append_dev(form, t16);
    			append_dev(form, input1);
    			set_input_value(input1, /*currEvent*/ ctx[0].aNumber);
    			append_dev(form, t17);
    			append_dev(form, label2);
    			append_dev(form, t19);
    			append_dev(form, input2);
    			set_input_value(input2, /*currEvent*/ ctx[0].bNumber);
    			append_dev(form, t20);
    			append_dev(form, textarea);
    			set_input_value(textarea, /*currEvent*/ ctx[0].sipInvite);
    			append_dev(article, t21);
    			append_dev(article, section0);
    			append_dev(section0, fieldset1);
    			append_dev(fieldset1, button4);
    			append_dev(section0, t23);
    			if (if_block0) if_block0.m(section0, null);
    			append_dev(article, t24);
    			append_dev(article, section1);
    			if_block1.m(section1, null);
    			append_dev(main, t25);
    			append_dev(main, footer);
    			append_dev(footer, t26);
    			append_dev(footer, a1);

    			if (!mounted) {
    				dispose = [
    					listen_dev(button0, "click", prevent_default(/*click_handler*/ ctx[5]), false, true, false),
    					listen_dev(button1, "click", prevent_default(/*click_handler_1*/ ctx[6]), false, true, false),
    					listen_dev(button2, "click", prevent_default(/*click_handler_2*/ ctx[7]), false, true, false),
    					listen_dev(button3, "click", prevent_default(/*click_handler_3*/ ctx[8]), false, true, false),
    					listen_dev(input0, "input", /*input0_input_handler*/ ctx[9]),
    					listen_dev(input1, "input", /*input1_input_handler*/ ctx[10]),
    					listen_dev(input2, "input", /*input2_input_handler*/ ctx[11]),
    					listen_dev(textarea, "input", /*textarea_input_handler*/ ctx[12]),
    					listen_dev(button4, "click", /*submitBatch*/ ctx[4], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*currEvent*/ 1 && input0.value !== /*currEvent*/ ctx[0].eventId) {
    				set_input_value(input0, /*currEvent*/ ctx[0].eventId);
    			}

    			if (dirty & /*currEvent*/ 1 && input1.value !== /*currEvent*/ ctx[0].aNumber) {
    				set_input_value(input1, /*currEvent*/ ctx[0].aNumber);
    			}

    			if (dirty & /*currEvent*/ 1 && input2.value !== /*currEvent*/ ctx[0].bNumber) {
    				set_input_value(input2, /*currEvent*/ ctx[0].bNumber);
    			}

    			if (dirty & /*currEvent*/ 1) {
    				set_input_value(textarea, /*currEvent*/ ctx[0].sipInvite);
    			}

    			if (/*requests*/ ctx[1].length > 0) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_3(ctx);
    					if_block0.c();
    					if_block0.m(section0, null);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block1) {
    				if_block1.p(ctx, dirty);
    			} else {
    				if_block1.d(1);
    				if_block1 = current_block_type(ctx);

    				if (if_block1) {
    					if_block1.c();
    					if_block1.m(section1, null);
    				}
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			if (if_block0) if_block0.d();
    			if_block1.d();
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Persephone', slots, []);

    	let genUUID = () => {
    		const RFC4122_TEMPLATE = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';

    		const replacePlaceholders = placeholder => {
    			const random = Math.floor(Math.random() * 16) % 16;
    			const value = placeholder === 'x' ? random : random & 0x3 | 0x8;
    			return value.toString(16);
    		};

    		return RFC4122_TEMPLATE.replace(/[xy]/g, replacePlaceholders);
    	};

    	let genPhoneNumber = () => {
    		return (Math.floor(Math.random() * 10000000000) % 10000000000).toString();
    	};

    	let makeEvent = () => {
    		let req = {
    			eventId: genUUID(),
    			aNumber: genPhoneNumber(),
    			bNumber: genPhoneNumber(),
    			sipInvite: 'BLAH BLAH BLAH'
    		};

    		return req;
    	};

    	let currEvent = makeEvent();

    	let addToBatch = count => {
    		if (responses.length > 0) {
    			$$invalidate(2, responses = []);
    		}

    		if (count) {
    			for (let i = 0; i < count - 1; i++) {
    				requests.push(makeEvent());
    			}
    		}

    		requests.push(currEvent);
    		$$invalidate(1, requests);
    		$$invalidate(0, currEvent = makeEvent());
    	};

    	let submitBatch = async e => {
    		e.preventDefault();

    		if (requests.length < 1) {
    			return;
    		}

    		let send = requests;
    		$$invalidate(1, requests = []);

    		let resp = await fetch('api/call-protection/batch', {
    			method: 'POST',
    			headers: { 'Content-Type': 'application/json' },
    			body: JSON.stringify(send)
    		});

    		resp = await resp.json();
    		$$invalidate(2, responses = resp.body);
    		$$invalidate(1, requests = []);
    	};

    	let requests = [];
    	let responses = [];
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Persephone> was created with unknown prop '${key}'`);
    	});

    	const click_handler = () => addToBatch();
    	const click_handler_1 = () => addToBatch(10);
    	const click_handler_2 = () => addToBatch(100);
    	const click_handler_3 = () => addToBatch(1000);

    	function input0_input_handler() {
    		currEvent.eventId = this.value;
    		$$invalidate(0, currEvent);
    	}

    	function input1_input_handler() {
    		currEvent.aNumber = this.value;
    		$$invalidate(0, currEvent);
    	}

    	function input2_input_handler() {
    		currEvent.bNumber = this.value;
    		$$invalidate(0, currEvent);
    	}

    	function textarea_input_handler() {
    		currEvent.sipInvite = this.value;
    		$$invalidate(0, currEvent);
    	}

    	$$self.$capture_state = () => ({
    		genUUID,
    		genPhoneNumber,
    		makeEvent,
    		currEvent,
    		addToBatch,
    		submitBatch,
    		requests,
    		responses
    	});

    	$$self.$inject_state = $$props => {
    		if ('genUUID' in $$props) genUUID = $$props.genUUID;
    		if ('genPhoneNumber' in $$props) genPhoneNumber = $$props.genPhoneNumber;
    		if ('makeEvent' in $$props) makeEvent = $$props.makeEvent;
    		if ('currEvent' in $$props) $$invalidate(0, currEvent = $$props.currEvent);
    		if ('addToBatch' in $$props) $$invalidate(3, addToBatch = $$props.addToBatch);
    		if ('submitBatch' in $$props) $$invalidate(4, submitBatch = $$props.submitBatch);
    		if ('requests' in $$props) $$invalidate(1, requests = $$props.requests);
    		if ('responses' in $$props) $$invalidate(2, responses = $$props.responses);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		currEvent,
    		requests,
    		responses,
    		addToBatch,
    		submitBatch,
    		click_handler,
    		click_handler_1,
    		click_handler_2,
    		click_handler_3,
    		input0_input_handler,
    		input1_input_handler,
    		input2_input_handler,
    		textarea_input_handler
    	];
    }

    class Persephone extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Persephone",
    			options,
    			id: create_fragment.name
    		});
    	}
    }

    const app = new Persephone({
        target: document.body,
    });

    return app;

})();
//# sourceMappingURL=bundle.js.map
