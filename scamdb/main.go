package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"scamdb/scamdbpb"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type server struct {
	scamdbpb.ScamDBServer
}

type mongoCategory struct {
	CategoryCode string `bson:"category_code"`
	CategoryName string `bson:"category_name"`
}

type mongoBlocklist struct {
	BlockedANumbers   []string `bson:"blocked_a_numbers"`
	BlockedCategories []string `bson:"blocked_categories"`
}

type mongoEvent struct {
	ANumber  string `bson:"a_number"`
	BNumber  string `bson:"b_number"`
	CallTime int64  `bson:"call_time"`
}

type mongoScoringResult struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	EventId   string             `bson:"event_id"`
	Event     mongoEvent         `bson:"event"`
	BlockCall bool               `bson:"block_call"`
	IsScam    bool               `bson:"is_scam"`
	Score     int                `bson:"score"`
}

var (
	mongoDBHost = getEnvOrElse("MONGODB_HOST", "localhost")
	mongoDBPort = getEnvOrElse("MONGODB_PORT", "27017")
)

func getEnvOrElse(key string, orElse string) string {
	val, exists := os.LookupEnv(key)
	if exists {
		return val
	}
	return orElse
}

var client *mongo.Client

func main() {
	ascii()
	log.Println("Starting ScamDB server...")
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	client, err = MongoInit(context.Background(), fmt.Sprintf("mongodb://%s:%s", mongoDBHost, mongoDBPort))
	if err != nil {
		log.Fatalf("Failed connect to mongodb: %v", err)
	}

	s := grpc.NewServer()
	scamdbpb.RegisterScamDBServer(s, &server{})
	reflection.Register(s)

	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(":7000", nil)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}

// MongoInit initializes a mongodb client given a context and a mongodb URI. Returns a pointer to the Mongo Client or
// an error if the connection is not established.
func MongoInit(ctx context.Context, mongoURI string) (*mongo.Client, error) {
	log.Println("Connecting to MongoDB")
	cl, err := mongo.NewClient(options.Client().ApplyURI(mongoURI))
	if err != nil {
		return nil, err
	}

	err = cl.Connect(ctx)
	if err != nil {
		return nil, err
	}
	return cl, nil
}

func (*server) GetPhoneCategory(ctx context.Context, req *scamdbpb.GetPhoneCategoryRequest) (*scamdbpb.GetPhoneCategoryResponse, error) {
	catColl := client.Database("categories").Collection("categories")
	number := req.GetPhoneNumber()

	var result mongoCategory
	err := catColl.FindOne(context.Background(), bson.D{{Key: "phone_number", Value: number}}).Decode(&result)
	if err != nil {
		log.Printf("Error searching for number: \"%v\" %v", number, err)
		return &scamdbpb.GetPhoneCategoryResponse{CategoryCode: "UN", CategoryName: "Unknown"}, nil
	}
	return &scamdbpb.GetPhoneCategoryResponse{CategoryCode: result.CategoryCode, CategoryName: result.CategoryName}, nil
}

func (*server) GetPersonalBlocklist(ctx context.Context, req *scamdbpb.GetPersonalBlocklistRequest) (*scamdbpb.GetPersonalBlocklistResponse, error) {
	blockColl := client.Database("blocklist").Collection("blocklist")
	number := req.GetPhoneNumber()

	var result mongoBlocklist
	err := blockColl.FindOne(context.Background(), bson.D{{Key: "phone_number", Value: number}}).Decode(&result)
	if err != nil {
		log.Printf("Error searching for number: \"%v\" %v", number, err)
		return &scamdbpb.GetPersonalBlocklistResponse{BlockedANumbers: nil, BlockedCategories: nil}, nil
	}
	return &scamdbpb.GetPersonalBlocklistResponse{BlockedANumbers: result.BlockedANumbers, BlockedCategories: result.BlockedCategories}, nil
}

func toMongo(req *scamdbpb.PutScoringResultRequest) mongoScoringResult {
	record := mongoScoringResult{
		EventId: req.GetScoringResult().GetEventId(),
		Event: mongoEvent{
			ANumber:  req.GetScoringResult().GetEvent().GetANumber(),
			BNumber:  req.GetScoringResult().GetEvent().GetBNumber(),
			CallTime: req.GetScoringResult().GetEvent().GetCallTime(),
		},
		BlockCall: req.GetScoringResult().GetBlockCall(),
		IsScam:    req.GetScoringResult().GetIsScam(),
		Score:     int(req.GetScoringResult().GetScore()),
	}
	return record
}

func fromMongo(result mongoScoringResult) *scamdbpb.ScoringResult {
	scoringResult := &scamdbpb.ScoringResult{
		EventId: result.EventId,
		Event: &scamdbpb.Event{
			ANumber:  result.Event.ANumber,
			BNumber:  result.Event.BNumber,
			CallTime: result.Event.CallTime,
		},
		BlockCall: result.BlockCall,
		IsScam:    result.IsScam,
		Score:     int64(result.Score),
	}
	return scoringResult
}

func (*server) PutScoringResult(ctx context.Context, req *scamdbpb.PutScoringResultRequest) (*scamdbpb.PutScoringResultResponse, error) {
	err := persist("scoringresults", toMongo(req))
	if err != nil {
		log.Printf("Error persisting scoring results: %v", err)
		return nil, err
	}
	return &scamdbpb.PutScoringResultResponse{}, nil
}

func persist(dbname string, record mongoScoringResult) error {
	coll := client.Database(dbname).Collection(dbname)
	_, err := coll.InsertOne(context.Background(), record)
	if err != nil {
		return err
	}
	return nil
}

func (*server) PutScoringResults(stream scamdbpb.ScamDB_PutScoringResultsServer) error {
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			log.Printf("Error while reading client stream: %v", err)
			return err
		}
		err = persist("scoringresults", toMongo(req))
		if err != nil {
			log.Printf("Error while persisting scoring results: %v", err)
			return err
		}
		sendErr := stream.Send(&scamdbpb.PutScoringResultResponse{})
		if sendErr != nil {
			log.Printf("Error while sending stream: %v", err)
			return err
		}
	}
}

func (*server) ListScoringResults(ctx context.Context, req *scamdbpb.ListScoringResultsRequest) (*scamdbpb.ListScoringResultsResponse, error) {
	limit := req.GetLimit()
	offset := req.GetOffset()
	timeFrom := req.GetTimeFrom()
	timeOffset := req.GetTimeOffset()
	pNumber := req.GetSearchPhoneNumber()

	var cur *mongo.Cursor
	var err error
	coll := client.Database("scoringresults").Collection("scoringresults")
	filter := bson.M{
		"$and": []bson.M{
			{
				"event.call_time": bson.M{
					"$gte": timeFrom - timeOffset,
				},
			},
			{
				"$or": []bson.M{
					{"event.a_number": bson.M{"$regex": pNumber}},
					{"event.b_number": bson.M{"$regex": pNumber}},
				},
			},
		},
	}
	cur, err = coll.Find(context.Background(), filter, &options.FindOptions{Limit: &limit, Skip: &offset})
	if err != nil {
		log.Printf("There was an error searching the collection: %v", err)
		return nil, err
	}
	defer cur.Close(context.Background())

	results := []*scamdbpb.ScoringResult{}

	for cur.Next(context.Background()) {
		result := mongoScoringResult{}
		err := cur.Decode(&result)
		if err != nil {
			log.Printf("Decode error: %v", err)
			return nil, err
		}
		results = append(results, fromMongo(result))
		log.Printf("Result: %v", result)
	}
	return &scamdbpb.ListScoringResultsResponse{ScoringResults: results}, nil
}

func (*server) UpdatePhoneNumber(ctx context.Context, req *scamdbpb.UpdatePhoneNumberRequest) (*scamdbpb.UpdatePhoneNumberResponse, error) {
	res := &scamdbpb.UpdatePhoneNumberResponse{}
	opts := options.Update().SetUpsert(true)

	catColl := client.Database("categories").Collection("categories")
	blockColl := client.Database("blocklist").Collection("blocklist")

	_, blockErr := blockColl.UpdateOne(ctx,
		bson.M{"phone_number": req.GetPhoneNumber()},
		bson.M{
			"$set": bson.M{
				"blocked_categories": req.GetBlockedCategories(),
				"blocked_a_numbers":  req.GetBlockedANumbers(),
			},
		}, opts)
	_, catErr := catColl.UpdateOne(ctx,
		bson.M{"phone_number": req.GetPhoneNumber()},
		bson.M{
			"$set": bson.M{
				"category_code": req.GetCategoryCode(),
				"category_name": req.GetCategoryName(),
			},
		}, opts)
	if blockErr != nil {
		log.Printf("There was an error updating the block list: %v", blockErr)
		return nil, blockErr
	} else if catErr != nil {
		log.Printf("There was an error updating the category list: %v", catErr)
		return nil, catErr
	}

	return res, nil
}

func (*server) SearchPhoneNumber(ctx context.Context, req *scamdbpb.SearchPhoneNumberRequest) (*scamdbpb.SearchPhoneNumberResponse, error) {
	opts := &options.FindOptions{}
	opts.SetProjection(bson.M{"phone_number": 1})
	var catRes []struct {
		PhoneNumber string `bson:"phone_number"`
	}
	var blockRes []struct {
		PhoneNumber string `bson:"phone_number"`
	}
	catColl := client.Database("categories").Collection("categories")
	catCur, catErr := catColl.Find(ctx,
		bson.M{
			"phone_number": primitive.Regex{
				Pattern: fmt.Sprintf("%v%v%v", "^", req.GetSearchTerm(), "$"), Options: "",
			},
		}, opts)
	if catErr != nil {
		log.Printf("Error while searching categories: %v", catErr)
		return nil, catErr
	}
	catCur.All(ctx, &catRes)

	blockColl := client.Database("blocklist").Collection("blocklist")
	blockCur, blockErr := blockColl.Find(ctx,
		bson.M{
			"phone_number": primitive.Regex{
				Pattern: fmt.Sprintf("%v%v%v", "^", req.GetSearchTerm(), "$"), Options: "",
			},
		}, opts)
	if blockErr != nil {
		log.Printf("Error while searching blocklist: %v", blockErr)
		return nil, blockErr
	}
	blockCur.All(ctx, &blockRes)
	resSlice := []string{}
	for _, res := range catRes {
		resSlice = append(resSlice, res.PhoneNumber)
	}
	for _, res := range blockRes {
		resSlice = append(resSlice, res.PhoneNumber)
	}

	return &scamdbpb.SearchPhoneNumberResponse{FoundNumbers: resSlice}, nil
}

func ascii() {
	fmt.Printf(" ▄ .▄ ▄▄▄· ·▄▄▄▄  ▄▄▄ ..▄▄ · \n██▪▐█▐█ ▀█ ██▪ ██ ▀▄.▀·▐█ ▀. \n██▀▐█▄█▀▀█ ▐█· ▐█▌▐▀▀▪▄▄▀▀▀█▄\n██▌▐▀▐█ ▪▐▌██. ██ ▐█▄▄▌▐█▄▪▐█\n▀▀▀ · ▀  ▀ ▀▀▀▀▀•  ▀▀▀  ▀▀▀▀\n")
}
