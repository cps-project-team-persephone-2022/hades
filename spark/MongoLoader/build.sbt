ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.12.10"

assembly / assemblyMergeStrategy := (_ => MergeStrategy.first)

lazy val root = (project in file("."))
  .settings(
    name := "MongoLoader"
  )

libraryDependencies += "org.apache.spark" %% "spark-core" % "3.1.2"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "3.1.2"
libraryDependencies += "org.mongodb.spark" %% "mongo-spark-connector" % "3.0.1"
libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "4.4.1"
libraryDependencies += "org.mongodb" % "mongo-java-driver" % "3.12.10"
libraryDependencies += "org.apache.spark" %% "spark-kubernetes" % "3.2.1"
